<?php
include_once 'navbar.php';
?>

<!--Product Detail Left Sidebar-->
<div class="header3 " style="background-color: #f7f8fb; height: 168px; padding: 50px 0px 50px 0px;">
    <div class="container">
        <div class="con " style="height: 168px; display: flex; justify-content: space-between">
            <div class="sidebartext">
                <h1>Shop Left Sidebar</h1>
            </div>
            <div class="">
                <div class="leftnav" style="display: flex">
                    <div class="leftnavlink">
                        <a href="">Home</a>
                        <span><i class="fa fa-angle-right "></i></span>
                    </div>
                    <div class="leftnavlink">
                        <a href="">Pages</a>
                        <span><i class="fa fa-angle-right "></i></span>
                    </div>
                    <div class="leftnavlink">
                        <a href="">Shop Left Sidebar</a>
                        <span><i class="fa fa-angle-right "></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Main content-->
<div class="mainbody" style="padding: 80px 0px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-12">
                        <div class="layout" style="display: flex; justify-content: space-between; margin-bottom: 1.5rem!important;">
                            <div class="select">
                                <select name="" id="">
                                    <option value="Default sorting">Default sorting</option>
                                    <option value="Sort by popularity">Sort by popularity</option>
                                    <option value="Sort by newness">Sort by newness</option>
                                    <option value="Sort by price: low to high">Sort by price: low to high</option>
                                    <option value="Sort by price: high to low">Sort by price: high to low</option>
                                </select>
                            </div>
                            <div class="tab" style="display: flex">
                                <div class="tabb " style="display: flex">
                                    <div class="grid t1">
                                        <a href="#" >
                                            <i class="ti-view-grid"></i>
                                        </a>
                                    </div>
                                    <div class="list t1">
                                        <a href="" ><i class="fa fa-list"></i></a>
                                    </div>
                                </div>
                                <div class="select2 t1">
                                    <select name="" id="">
                                        <option value="Showing">Showing</option>
                                        <option value="9">9</option>
                                        <option value="12">12</option>
                                        <option value="18">18</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="box " >
                            <div class="inner" id="inner" style="display: none">
                                <div class="row">
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="inner-box" >
                                            <div class="productBox">
                                                <div class="details">
                                                    <div class="img">
                                                        <a href="sliderexample.php">
                                                            <img  src="img/img2.jpg" alt="">
                                                        </a>
                                                        <div class="cover">
                                                            <div class="icon" >
                                                                <a href=""><i class="fa fa-shopping-cart" ></i></a>
                                                                <a href=""> <i class="fa fa-random"></i></i></a>
                                                                <a href=""><i class="fa fa-search-plus"></i></a>
                                                                <a href=""><i class="fa fa-heart" ></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="info">
                                                        <div class="text">
                                                            <h6><a href="sliderexample.php">Blue Dress For Woman</a></h6>
                                                        </div>
                                                        <div class="sales">
                                                            <span class="s1">$45.00</span>
                                                            <div class="s2">$89.00</div>
                                                            <div class="s3"><span>35% off</span></div>
                                                        </div>
                                                        <div class="starRate">
                                                            <div class="star">
                                                    <span class="stars">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </span>
                                                    <span class="st" ><i class="fa fa-star-o"></i></span>
                                                    </div>
                                                    <div class="num">
                                                        (25)
                                                    </div>
                                                    <div class="circle">
                                                        <div class="circle-box">
                                                            <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                            <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                            <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                            <span class="c1" style="background-color: rgb(3, 147, 181);"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="inner-box" >
                                            <div class="productBox">
                                                <div class="details">
                                                    <div class="img">
                                                        <a href="sliderexample.php">
                                                            <img  src="img/img3.jpg" alt="">
                                                        </a>
                                                        <div class="cover">
                                                            <div class="icon" >
                                                                <a href=""><i class="fa fa-shopping-cart" ></i></a>
                                                                <a href=""> <i class="fa fa-random"></i></i></a>
                                                                <a href=""><i class="fa fa-search-plus"></i></a>
                                                                <a href=""><i class="fa fa-heart" ></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="info">
                                                        <div class="text">
                                                            <h6><a href="sliderexample.php">Blue Dress For Woman</a></h6>
                                                        </div>
                                                        <div class="sales">
                                                            <span class="s1">$45.00</span>
                                                            <div class="s2">$89.00</div>
                                                            <div class="s3"><span>35% off</span></div>
                                                        </div>
                                                        <div class="starRate">
                                                            <div class="star">
                                                <span class="stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </span>
                                                                <span class="st" ><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <div class="num">
                                                                (25)
                                                            </div>
                                                            <div class="circle">
                                                                <div class="circle-box">
                                                                    <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                                    <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                                    <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                                    <span class="c1" style="background-color: rgb(3, 147, 181);"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="inner-box" >
                                            <div class="productBox">
                                                <div class="details">
                                                    <div class="img">
                                                        <a href="sliderexample.php">
                                                            <img  src="img/img4.jpg" alt="">
                                                        </a>
                                                        <div class="cover">
                                                            <div class="icon" >
                                                                <a href=""><i class="fa fa-shopping-cart" ></i></a>
                                                                <a href=""> <i class="fa fa-random"></i></i></a>
                                                                <a href=""><i class="fa fa-search-plus"></i></a>
                                                                <a href=""><i class="fa fa-heart" ></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="info">
                                                        <div class="text">
                                                            <h6><a href="sliderexample.php">Blue Dress For Woman</a></h6>
                                                        </div>
                                                        <div class="sales">
                                                            <span class="s1">$45.00</span>
                                                            <div class="s2">$89.00</div>
                                                            <div class="s3"><span>35% off</span></div>
                                                        </div>
                                                        <div class="starRate">
                                                        <div class="star">
                                                            <span class="stars">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </span>
                                                            <span class="st" ><i class="fa fa-star-o"></i></span>
                                                        </div>
                                                        <div class="num">
                                                            (25)
                                                        </div>
                                                        <div class="circle">
                                                            <div class="circle-box">
                                                                <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                                <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                                <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                                <span class="c1" style="background-color: rgb(3, 147, 181);"></span>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="inner-box" >
                                            <div class="productBox">
                                                <div class="details">
                                                    <div class="img">
                                                        <a href="sliderexample.php">
                                                            <img  src="img/img5.jpg" alt="">
                                                        </a>
                                                        <div class="cover">
                                                            <div class="icon" >
                                                                <a href=""><i class="fa fa-shopping-cart" ></i></a>
                                                                <a href=""> <i class="fa fa-random"></i></i></a>
                                                                <a href=""><i class="fa fa-search-plus"></i></a>
                                                                <a href=""><i class="fa fa-heart" ></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="info">
                                                        <div class="text">
                                                            <h6><a href="sliderexample.php">Blue Dress For Woman</a></h6>
                                                        </div>
                                                        <div class="sales">
                                                            <span class="s1">$45.00</span>
                                                            <div class="s2">$89.00</div>
                                                            <div class="s3"><span>35% off</span></div>
                                                        </div>
                                                        <div class="starRate">
                                                            <div class="star">
                                                <span class="stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </span>
                                                                <span class="st" ><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <div class="num">
                                                                (25)
                                                            </div>
                                                            <div class="circle">
                                                                <div class="circle-box">
                                                                    <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                                    <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                                    <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                                    <span class="c1" style="background-color: rgb(3, 147, 181);"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="inner-box" >
                                            <div class="productBox">
                                                <div class="details">
                                                    <div class="img">
                                                        <a href="sliderexample.php">
                                                            <img  src="img/img6.jpg" alt="">
                                                        </a>
                                                        <div class="cover">
                                                            <div class="icon" >
                                                                <a href=""><i class="fa fa-shopping-cart" ></i></a>
                                                                <a href=""> <i class="fa fa-random"></i></i></a>
                                                                <a href=""><i class="fa fa-search-plus"></i></a>
                                                                <a href=""><i class="fa fa-heart" ></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="info">
                                                        <div class="text">
                                                            <h6><a href="sliderexample.php">Blue Dress For Woman</a></h6>
                                                        </div>
                                                        <div class="sales">
                                                            <span class="s1">$45.00</span>
                                                            <div class="s2">$89.00</div>
                                                            <div class="s3"><span>35% off</span></div>
                                                        </div>
                                                        <div class="starRate">
                                                            <div class="star">
                                                <span class="stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </span>
                                                                <span class="st" ><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <div class="num">
                                                                (25)
                                                            </div>
                                                            <div class="circle">
                                                                <div class="circle-box">
                                                                    <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                                    <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                                    <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                                    <span class="c1" style="background-color: rgb(3, 147, 181);"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="inner-box" >
                                            <div class="productBox">
                                                <div class="details">
                                                    <div class="img">
                                                        <a href="sliderexample.php">
                                                            <img  src="img/img9.jpg" alt="">
                                                        </a>
                                                        <div class="cover">
                                                            <div class="icon" >
                                                                <a href=""><i class="fa fa-shopping-cart" ></i></a>
                                                                <a href=""> <i class="fa fa-random"></i></i></a>
                                                                <a href=""><i class="fa fa-search-plus"></i></a>
                                                                <a href=""><i class="fa fa-heart" ></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="info">
                                                        <div class="text">
                                                            <h6><a href="sliderexample.php">Blue Dress For Woman</a></h6>
                                                        </div>
                                                        <div class="sales">
                                                            <span class="s1">$45.00</span>
                                                            <div class="s2">$89.00</div>
                                                            <div class="s3"><span>35% off</span></div>
                                                        </div>
                                                        <div class="starRate">
                                                            <div class="star">
                                                <span class="stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </span>
                                                                <span class="st" ><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <div class="num">
                                                                (25)
                                                            </div>
                                                            <div class="circle">
                                                                <div class="circle-box">
                                                                    <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                                    <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                                    <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                                    <span class="c1" style="background-color: rgb(3, 147, 181);"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="inner-box" >
                                            <div class="productBox">
                                                <div class="details">
                                                    <div class="img">
                                                        <a href="sliderexample.php">
                                                            <img  src="img/img7.jpg" alt="">
                                                        </a>
                                                        <div class="cover">
                                                            <div class="icon" >
                                                                <a href=""><i class="fa fa-shopping-cart" ></i></a>
                                                                <a href=""> <i class="fa fa-random"></i></i></a>
                                                                <a href=""><i class="fa fa-search-plus"></i></a>
                                                                <a href=""><i class="fa fa-heart" ></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="info">
                                                        <div class="text">
                                                            <h6><a href="sliderexample.php">Blue Dress For Woman</a></h6>
                                                        </div>
                                                        <div class="sales">
                                                            <span class="s1">$45.00</span>
                                                            <div class="s2">$89.00</div>
                                                            <div class="s3"><span>35% off</span></div>
                                                        </div>
                                                        <div class="starRate">
                                                            <div class="star">
                                                <span class="stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </span>
                                                                <span class="st" ><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <div class="num">
                                                                (25)
                                                            </div>
                                                            <div class="circle">
                                                                <div class="circle-box">
                                                                    <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                                    <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                                    <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                                    <span class="c1" style="background-color: rgb(3, 147, 181);"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="inner-box" >
                                            <div class="productBox">
                                                <div class="details">
                                                    <div class="img">
                                                        <a href="sliderexample.php">
                                                            <img  src="img/img8.jpg" alt="">
                                                        </a>
                                                        <div class="cover">
                                                            <div class="icon" >
                                                                <a href=""><i class="fa fa-shopping-cart" ></i></a>
                                                                <a href=""> <i class="fa fa-random"></i></i></a>
                                                                <a href=""><i class="fa fa-search-plus"></i></a>
                                                                <a href=""><i class="fa fa-heart" ></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="info">
                                                        <div class="text">
                                                            <h6><a href="sliderexample.php">Blue Dress For Woman</a></h6>
                                                        </div>
                                                        <div class="sales">
                                                            <span class="s1">$45.00</span>
                                                            <div class="s2">$89.00</div>
                                                            <div class="s3"><span>35% off</span></div>
                                                        </div>
                                                        <div class="starRate">
                                                            <div class="star">
                                                <span class="stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </span>
                                                                <span class="st" ><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <div class="num">
                                                                (25)
                                                            </div>
                                                            <div class="circle">
                                                                <div class="circle-box">
                                                                    <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                                    <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                                    <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                                    <span class="c1" style="background-color: rgb(3, 147, 181);"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="inner-box" >
                                            <div class="productBox">
                                                <div class="details">
                                                    <div class="img">
                                                        <a href="sliderexample.php">
                                                            <img  src="img/img9.jpg" alt="">
                                                        </a>
                                                        <div class="cover">
                                                            <div class="icon" >
                                                                <a href=""><i class="fa fa-shopping-cart" ></i></a>
                                                                <a href=""> <i class="fa fa-random"></i></i></a>
                                                                <a href=""><i class="fa fa-search-plus"></i></a>
                                                                <a href=""><i class="fa fa-heart" ></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="info">
                                                        <div class="text">
                                                            <h6><a href="sliderexample.php">Blue Dress For Woman</a></h6>
                                                        </div>
                                                        <div class="sales">
                                                            <span class="s1">$45.00</span>
                                                            <div class="s2">$89.00</div>
                                                            <div class="s3"><span>35% off</span></div>
                                                        </div>
                                                        <div class="starRate">
                                                            <div class="star">
                                                <span class="stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </span>
                                                                <span class="st" style="display: none"><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <div class="num">
                                                                (25)
                                                            </div>
                                                            <div class="circle">
                                                                <div class="circle-box">
                                                                    <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                                    <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                                    <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                                    <span class="c1" style="background-color: rgb(3, 147, 181);"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="innerlist ">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="listcontainer">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4" style="padding-right: 0px !important;">
                                                    <div class="listimg">
                                                        <a href="">
                                                            <img src="img/img1.jpg" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="listinfo">
                                                        <div class="listheader">
                                                            <a href="">
                                                                <h6>Blue Dress For Women</h6>
                                                            </a>
                                                        </div>
                                                        <div class="listprice">
                                                            <div class="lprice">
                                                                <span style="color: #FF324D; font-weight: 700;">$45.00</span>
                                                                <span class="listdelete">$55.25</span>
                                                                <span class="listsale">35% off</span>
                                                            </div>
                                                            <div class="listrating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <span style="margin-left: 5px; font-size: 14px">(21)</span>
                                                            </div>
                                                        </div>
                                                        <div class="listdetails">
                                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc id varius nunc.</p>
                                                        </div>
                                                        <div class="listcolor circle-box">
                                                            <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                            <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                            <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                        </div>
                                                        <div class="listaction">
                                                            <div class="listactionholder" >
                                                                <div class="listaddtcart">
                                                                    <a href="">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                        <span>Add To Cart</span>
                                                                    </a>
                                                                </div>
                                                                <div class="listicons">
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-heart"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12"></div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="listcontainer">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4" style="padding-right: 0px !important;">
                                                    <div class="listimg">
                                                        <a href="">
                                                            <img src="img/img2.jpg" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="listinfo">
                                                        <div class="listheader">
                                                            <a href="">
                                                                <h6>Blue Dress For Women</h6>
                                                            </a>
                                                        </div>
                                                        <div class="listprice">
                                                            <div class="lprice">
                                                                <span style="color: #FF324D; font-weight: 700;">$45.00</span>
                                                                <span class="listdelete">$55.25</span>
                                                                <span class="listsale">35% off</span>
                                                            </div>
                                                            <div class="listrating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <span style="margin-left: 5px; font-size: 14px">(21)</span>
                                                            </div>
                                                        </div>
                                                        <div class="listdetails">
                                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc id varius nunc.</p>
                                                        </div>
                                                        <div class="listcolor circle-box">
                                                            <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                            <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                            <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                        </div>
                                                        <div class="listaction">
                                                            <div class="listactionholder" >
                                                                <div class="listaddtcart">
                                                                    <a href="">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                        <span>Add To Cart</span>
                                                                    </a>
                                                                </div>
                                                                <div class="listicons">
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-heart"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12"></div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="listcontainer">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4" style="padding-right: 0px !important;">
                                                    <div class="listimg">
                                                        <a href="">
                                                            <img src="img/img3.jpg" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="listinfo">
                                                        <div class="listheader">
                                                            <a href="">
                                                                <h6>Blue Dress For Women</h6>
                                                            </a>
                                                        </div>
                                                        <div class="listprice">
                                                            <div class="lprice">
                                                                <span style="color: #FF324D; font-weight: 700;">$45.00</span>
                                                                <span class="listdelete">$55.25</span>
                                                                <span class="listsale">35% off</span>
                                                            </div>
                                                            <div class="listrating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <span style="margin-left: 5px; font-size: 14px">(21)</span>
                                                            </div>
                                                        </div>
                                                        <div class="listdetails">
                                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc id varius nunc.</p>
                                                        </div>
                                                        <div class="listcolor circle-box">
                                                            <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                            <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                            <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                        </div>
                                                        <div class="listaction">
                                                            <div class="listactionholder" >
                                                                <div class="listaddtcart">
                                                                    <a href="">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                        <span>Add To Cart</span>
                                                                    </a>
                                                                </div>
                                                                <div class="listicons">
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-heart"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12"></div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="listcontainer">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4" style="padding-right: 0px !important;">
                                                    <div class="listimg">
                                                        <a href="">
                                                            <img src="img/img4.jpg" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="listinfo">
                                                        <div class="listheader">
                                                            <a href="">
                                                                <h6>Blue Dress For Women</h6>
                                                            </a>
                                                        </div>
                                                        <div class="listprice">
                                                            <div class="lprice">
                                                                <span style="color: #FF324D; font-weight: 700;">$45.00</span>
                                                                <span class="listdelete">$55.25</span>
                                                                <span class="listsale">35% off</span>
                                                            </div>
                                                            <div class="listrating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <span style="margin-left: 5px; font-size: 14px">(21)</span>
                                                            </div>
                                                        </div>
                                                        <div class="listdetails">
                                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc id varius nunc.</p>
                                                        </div>
                                                        <div class="listcolor circle-box">
                                                            <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                            <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                            <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                        </div>
                                                        <div class="listaction">
                                                            <div class="listactionholder" >
                                                                <div class="listaddtcart">
                                                                    <a href="">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                        <span>Add To Cart</span>
                                                                    </a>
                                                                </div>
                                                                <div class="listicons">
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-heart"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12"></div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="listcontainer">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4" style="padding-right: 0px !important;">
                                                    <div class="listimg">
                                                        <a href="">
                                                            <img src="img/img5.jpg" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="listinfo">
                                                        <div class="listheader">
                                                            <a href="">
                                                                <h6>Blue Dress For Women</h6>
                                                            </a>
                                                        </div>
                                                        <div class="listprice">
                                                            <div class="lprice">
                                                                <span style="color: #FF324D; font-weight: 700;">$45.00</span>
                                                                <span class="listdelete">$55.25</span>
                                                                <span class="listsale">35% off</span>
                                                            </div>
                                                            <div class="listrating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <span style="margin-left: 5px; font-size: 14px">(21)</span>
                                                            </div>
                                                        </div>
                                                        <div class="listdetails">
                                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc id varius nunc.</p>
                                                        </div>
                                                        <div class="listcolor circle-box">
                                                            <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                            <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                            <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                        </div>
                                                        <div class="listaction">
                                                            <div class="listactionholder" >
                                                                <div class="listaddtcart">
                                                                    <a href="">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                        <span>Add To Cart</span>
                                                                    </a>
                                                                </div>
                                                                <div class="listicons">
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-heart"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12"></div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="listcontainer">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4" style="padding-right: 0px !important;">
                                                    <div class="listimg">
                                                        <a href="">
                                                            <img src="img/img6.jpg" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="listinfo">
                                                        <div class="listheader">
                                                            <a href="">
                                                                <h6>Blue Dress For Women</h6>
                                                            </a>
                                                        </div>
                                                        <div class="listprice">
                                                            <div class="lprice">
                                                                <span style="color: #FF324D; font-weight: 700;">$45.00</span>
                                                                <span class="listdelete">$55.25</span>
                                                                <span class="listsale">35% off</span>
                                                            </div>
                                                            <div class="listrating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <span style="margin-left: 5px; font-size: 14px">(21)</span>
                                                            </div>
                                                        </div>
                                                        <div class="listdetails">
                                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc id varius nunc.</p>
                                                        </div>
                                                        <div class="listcolor circle-box">
                                                            <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                            <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                            <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                        </div>
                                                        <div class="listaction">
                                                            <div class="listactionholder" >
                                                                <div class="listaddtcart">
                                                                    <a href="">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                        <span>Add To Cart</span>
                                                                    </a>
                                                                </div>
                                                                <div class="listicons">
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-heart"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12"></div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="listcontainer">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4" style="padding-right: 0px !important;">
                                                    <div class="listimg">
                                                        <a href="">
                                                            <img src="img/img7.jpg" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="listinfo">
                                                        <div class="listheader">
                                                            <a href="">
                                                                <h6>Blue Dress For Women</h6>
                                                            </a>
                                                        </div>
                                                        <div class="listprice">
                                                            <div class="lprice">
                                                                <span style="color: #FF324D; font-weight: 700;">$45.00</span>
                                                                <span class="listdelete">$55.25</span>
                                                                <span class="listsale">35% off</span>
                                                            </div>
                                                            <div class="listrating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <span style="margin-left: 5px; font-size: 14px">(21)</span>
                                                            </div>
                                                        </div>
                                                        <div class="listdetails">
                                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc id varius nunc.</p>
                                                        </div>
                                                        <div class="listcolor circle-box">
                                                            <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                            <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                            <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                        </div>
                                                        <div class="listaction">
                                                            <div class="listactionholder" >
                                                                <div class="listaddtcart">
                                                                    <a href="">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                        <span>Add To Cart</span>
                                                                    </a>
                                                                </div>
                                                                <div class="listicons">
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-heart"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12"></div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="listcontainer">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4" style="padding-right: 0px !important;">
                                                    <div class="listimg">
                                                        <a href="">
                                                            <img src="img/img8.jpg" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="listinfo">
                                                        <div class="listheader">
                                                            <a href="">
                                                                <h6>Blue Dress For Women</h6>
                                                            </a>
                                                        </div>
                                                        <div class="listprice">
                                                            <div class="lprice">
                                                                <span style="color: #FF324D; font-weight: 700;">$45.00</span>
                                                                <span class="listdelete">$55.25</span>
                                                                <span class="listsale">35% off</span>
                                                            </div>
                                                            <div class="listrating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <span style="margin-left: 5px; font-size: 14px">(21)</span>
                                                            </div>
                                                        </div>
                                                        <div class="listdetails">
                                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc id varius nunc.</p>
                                                        </div>
                                                        <div class="listcolor circle-box">
                                                            <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                            <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                            <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                        </div>
                                                        <div class="listaction">
                                                            <div class="listactionholder" >
                                                                <div class="listaddtcart">
                                                                    <a href="">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                        <span>Add To Cart</span>
                                                                    </a>
                                                                </div>
                                                                <div class="listicons">
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-heart"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12"></div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="listcontainer">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4" style="padding-right: 0px !important;">
                                                    <div class="listimg">
                                                        <a href="">
                                                            <img src="img/img9.jpg" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="listinfo">
                                                        <div class="listheader">
                                                            <a href="">
                                                                <h6>Blue Dress For Women</h6>
                                                            </a>
                                                        </div>
                                                        <div class="listprice">
                                                            <div class="lprice">
                                                                <span style="color: #FF324D; font-weight: 700;">$45.00</span>
                                                                <span class="listdelete">$55.25</span>
                                                                <span class="listsale">35% off</span>
                                                            </div>
                                                            <div class="listrating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <span style="margin-left: 5px; font-size: 14px">(21)</span>
                                                            </div>
                                                        </div>
                                                        <div class="listdetails">
                                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc id varius nunc.</p>
                                                        </div>
                                                        <div class="listcolor circle-box">
                                                            <span class="c1" style="background-color: rgb(135, 85, 75);"></span>
                                                            <span class="c1" style="background-color: rgb(51, 51, 51)"></span>
                                                            <span class="c1" style="background-color: rgb(218, 50, 63)"></span>
                                                        </div>
                                                        <div class="listaction">
                                                            <div class="listactionholder" >
                                                                <div class="listaddtcart">
                                                                    <a href="">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                        <span>Add To Cart</span>
                                                                    </a>
                                                                </div>
                                                                <div class="listicons">
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-control-shuffle"></i></a>
                                                                    <a href="" class="shuff"><i class="ti-heart"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <ul class="pagination">
                            <li>
                                <a href="#">1</a>
                            </li>
                            <li>
                                <a href="#">2</a>
                            </li>
                            <li>
                                <a href="#">3</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-arrow-right"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3" >
                <div class="sidebar">
                   <div class="categories">
                       <h5>Categories</h5>
                       <div class="categoriesDetails">
                           <div class="categoriesD">
                               <a href="">
                                   <i class="fa fa-angle-right"></i>
                                   <span>Women</span>
                               </a>
                               <span class="categoriesnum">(9)</span>
                           </div>
                           <div class="categoriesD">
                               <a href="">
                                   <i class="fa fa-angle-right"></i>
                                   <span>Top</span>
                               </a>
                               <span class="categoriesnum"> (9)</span>
                           </div>
                           <div class="categoriesD">
                               <a href="">
                                   <i class="fa fa-angle-right"></i>
                                   <span>T-shirts</span>
                               </a>
                               <span class="categoriesnum">(12)</span>
                           </div>
                           <div class="categoriesD">
                               <a href="">
                                   <i class="fa fa-angle-right"></i>
                                   <span>Men</span>
                               </a>
                               <span class="categoriesnum">(7)</span>
                           </div>
                           <div class="categoriesD">
                               <a href="">
                                   <i class="fa fa-angle-right"></i>
                                   <span>Shoes</span>
                               </a>
                               <span class="categoriesnum">(12)</span>
                           </div>
                       </div>
                   </div>
                    <div class="filter categories">
                        <div class="filterTitle">
                            <h5>Filter</h5>
                        </div>
                        <div class="filterPrice">

                        </div>
                        <div class="filterPriceRange">
                            <span>
                                Price:
                                <span class="Fprice">
                                    $50 - $300
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="brand categories">
                        <div class="brandtiltle">
                            <h5>Brand</h5>
                        </div>
                        <div class="brandcheckbox">
                            <div class="brandcheck">
                                <span><i class="fa fa-square-o"></i></span>
                                <span>New Arrivals</span>
                            </div>
                            <div class="brandcheck">
                                <span><i class="fa fa-square-o"></i></span>
                                <span>Lighting</span>
                            </div>
                            <div class="brandcheck">
                                <span><i class="fa fa-square-o"></i></span>
                                <span>Tables</span>
                            </div>
                            <div class="brandcheck">
                                <span><i class="fa fa-square-o"></i></span>
                                <span>Chairs</span>
                            </div>
                            <div class="brandcheck">
                                <span><i class="fa fa-square-o"></i></span>
                                <span>Accessories</span>
                            </div>
                        </div>
                    </div>
                    <div class="size categories">
                        <h5>Size</h5>
                        <div class="sizes">
                            <span>XS</span>
                            <span>S</span>
                            <span class="active">M</span>
                            <span>L</span>
                            <span>XL</span>
                            <span>2XL</span>
                            <span>3XL</span>

                        </div>
                    </div>
                    <div class="cat-colour">
                        <h5>Color</h5>
                        <span style="background-color: rgb(135, 85, 75);"></span>
                        <span style="background-color: rgb(51, 51, 51);"></span>
                        <span style="background-color: rgb(218, 50, 63);"></span>
                        <span style="background-color: rgb(47, 54, 108);"></span>
                        <span style="background-color: rgb(181, 182, 187);"></span>
                        <span style="background-color: rgb(185, 194, 223);"></span>
                        <span style="background-color: rgb(95, 183, 212);"></span>
                        <span style="background-color: rgb(47, 54, 108);"></span>
                    </div>
                    <div class="sidebarBan">
                        <div class="sidebanImg" style="position: relative">
                            <img src="img/sidebar_banner_img.jpg" alt="">
                        </div>
                        <div class="sidebanInfo">
                            <h5>NEW COLLECTION</h5>
                            <h3>SALE 30% OFF</h3>
                            <div class="button">
                                <a href="" class="btn">SHOP NOW</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>


