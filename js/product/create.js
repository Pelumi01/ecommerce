$(document).ready(function() {
    $('.admin_log').hover(function() {
        $('.drop-log').toggle(300);
    })
});

function consoleLog() {
    console.log("Clicked");
}




$('#submit').click(function(event){
    var file = document.getElementById('file').files[0];
    var item = $('#item').val();
    var price = $('#price').val();
    var cancleP = $('#cancleP').val();
    var discount = $('#discount').val();


    var form_data = new FormData();
    event.preventDefault();
    form_data.append('item',item);
    form_data.append('price', price);
    form_data.append('cancleP',cancleP);
    form_data.append('discount', discount);
    form_data.append('file', file);


    $.ajax({
        url:"../handlers/products.php?create-item",
        method:'POST',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        success:function(data){
            console.log(data);
            var resp=JSON.parse(data);
            if(resp.status == 422){
                $("#error").html(resp.message);
            }
            if(resp.status == 200){
                localStorage.setItem('success',resp.message);
                window.location.href='products.php';
            }

        },
        error:function(error){
            console.log(error);
        }
    });

});