$(document).ready(function() {
    $('.admin_log').hover(function() {
        $('.drop-log').toggle(300);
    })



    $('#submit_edit').click(function(event){
        event.preventDefault();
        var file = document.getElementById('file').files[0];
        var item = $('#item').val();
        var price = $('#price').val();
        var cancleP = $('#cancleP').val();
        var discount = $('#discount').val();
        var id = $('#id').val();
        console.log(id);
        var form_data = new FormData();
        event.preventDefault();
        form_data.append('item',item);
        form_data.append('price', price);
        form_data.append('cancleP',cancleP);
        form_data.append('discount', discount);
        form_data.append('file', file);
        form_data.append('id', id);

        $.ajax({
            url:"../handlers/products.php?edit="+id,
            method:'POST',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success:function(data){
                console.log(data);
                var resp=JSON.parse(data);
                if(resp.status == 422){
                    $("#error").html(resp.message);
                }
                if(resp.status == 200){
                    // console.log('done');
                    localStorage.setItem('success',resp.message);
                    window.location.href='products.php';
                }
            }
        });
    });
});

function consoleLog() {
    console.log("Clicked");
}