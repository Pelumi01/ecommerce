$(document).ready(function() {
    var message=localStorage.getItem('success');
    if(message != null){
        $("#success").html(message);

        localStorage.removeItem('success');
    }
    $('.admin_log').hover(function() {
        $('.drop-log').toggle(300);
    })

    $('.delete-products').click(function(event){
        var delete_id = $(this).attr('data-id');
        console.log(delete_id);
        $.ajax({
            url: "../handlers/products.php",
            method: 'POST',
            data: {
                'delete': delete_id
            },
            success:function(data){
                console.log(data);
                var resp=JSON.parse(data);
                if(resp.status == 200){
                    console.log('done');
                    $('#prod'+delete_id).remove();
                    $("#success").html(resp.message);

                    // window.location.reload();
                }
            }
        });
    });
});

function consoleLog() {
    console.log("Clicked");
}

console.log(window.location);