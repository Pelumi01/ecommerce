<?php
session_start();
require_once('./config/admin_db_handler.php');
if (isset($_POST['submit'])) {

    $username = $_POST['email'];
    $password = $_POST['password'];
    Logadmin::adminCheck($username, $password);
    $_SESSION['username'] = $username;
    $_SESSION['password'] = $password;
}
include_once 'navbar.php';
?>
<!--Product Detail Left Sidebar-->
<div class="header3 " style="background-color: #f7f8fb; height: 168px; padding: 50px 0px 50px 0px;">
    <div class="container">
        <div class="con " style="height: 168px; display: flex; justify-content: space-between">
            <div class="sidebartext">
                <h1>Login</h1>
            </div>
            <div>
                <div class="leftnav" style="display: flex">
                    <div class="leftnavlink">
                        <a href="">Home</a>
                        <span><i class="fa fa-angle-right "></i></span>
                    </div>
                    <div class="leftnavlink">
                        <a href="">Pages</a>
                        <span><i class="fa fa-angle-right "></i></span>
                    </div>
                    <div class="leftnavlink">
                        <a href="">Shop Left Sidebar</a>
                        <span><i class="fa fa-angle-right "></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Main content-->
<div class="loginSection text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="loginContainer ">
                    <div class="loginInner">
                        <div class="loginHeader">
                            <h3>Login</h3>
                        </div>
                        <form action="" method="POST">
                            <?php if (isset($_SESSION['error'])) : ?>
                                <div class="alert alert-danger" role="alert">
                                    <?php echo $_SESSION['error'];
                                    unset($_SESSION['error']);
                                    ?>
                                </div>
                            <?php endif ?>
                            <input class="loginInp" name="email" type="text" placeholder="Your Username"><br>
                            <input class="loginInp" name="password" type="password" placeholder="Password"><br>
                            <div class="loginfooter">
                                <div>
                                    <input type="checkbox" name="checkbox">
                                    <span>Remember me</span>
                                </div>
                            </div>
                            <button class="btn inpb Loginbut" name="submit" type="submit"><span>Log In</span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>