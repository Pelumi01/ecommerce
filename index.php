<?php
include_once 'config/db.php';
include_once 'config/crud_handler.php';

?>


<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="./dist/plugin/owl.carousel.css" rel="stylesheet">
	<link href="./dist/plugin/owl.theme.default.css" rel="stylesheet">
	<link rel="stylesheet" href="./dist/plugin/bootstrap.css">
	<link rel="stylesheet" href="./dist/css/main_style.min.css">
	<link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
	<script src="https://kit.fontawesome.com/dba3c70e5f.js"></script>
	<title>Home</title>
</head>

<body>
	<!-- PRELOADER -->
	<div id="preloader" style="display:none;">
		<div class="loader-balls">
			<span class="b-1"></span>
			<span class="b-2"></span>
			<span class="b-3"></span>
		</div>
	</div>

	<!-- Start of the header -->
	<header>
		<div class="header-top d-none d-md-block">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-lg-6">
						<div class="header-info">
							<div class="com-benefit">
								<span>Free Ground Shipping Over $250</span>
							</div>
							<div class="app-wrapper ">
								<span class="mr-3">Download App</span>
								<ul class="d-flex justify-content-lg-around align-items-center text-center text-lg-left mb-0">
									<li><a href="#"><i class="fab fa-apple"></i></a></li>
									<li><a href="#"><i class="fab fa-android"></i></a></li>
									<li><a href="#"><i class="fab fa-windows"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-lg-6">
						<div class="h-select ">
							<div class="lang_dropdown">
								<div class="ddOutOfVision" id="msdrpdd20_msddHolder">
									<select name="countries" class="lang_select">
										<option value="en" data-title="English"><span><img src="./img/eng.png" alt=""></span>English</option>
										<option value="fn" data-title="France"><span><img src="./img/fn.png" alt=""></span>France</option>
										<option value="us" data-title="United States"><img src="./img/us.png" alt=""><span></span> U S A</option>
									</select>
								</div>
								<!-- <div class="dd ddcommon borderRadius">
									<div class="ddTitle borderRadiusTp"><span class="divider"></span><span class="ddArrow arrowoff"></span><span class="ddTitleText " id="msdrpdd20_title"><img src="img/eng.png" class="fnone"><span class="ddlabel">English</span><span class="description" style="display: none;"></span></span></div><input id="msdrpdd20_titleText" type="text" autocomplete="off" class="text shadow borderRadius" style="display: none;">
									<div class="ddChild ddchild_ border shadow" id="msdrpdd20_child" style="z-index: 1; display: none; position: absolute; visibility: visible; height: 3px; top: 32px;">
										<ul>
											<li class="enabled _msddli_ selected" title="English"><img src="./img/eng.png" class="fnone"><span class="ddlabel">English</span>
												<div class="clear"></div>
											</li>
											<li class="enabled _msddli_" title="France"><img src="./img/fn.png" class="fnone"><span class="ddlabel">France</span>
												<div class="clear"></div>
											</li>
											<li class="enabled _msddli_" title="United States"><img src="./img/us.png" class="fnone"><span class="ddlabel">United States</span>
												<div class="clear"></div>
											</li>
										</ul>
									</div>
								</div> -->
							</div>
							<div class="ddOutOfVision" id="msdrpdd21_msddHolder">
								<select name="countries" class="custome_select" id="msdrpdd21" tabindex="-1">
									<option value="USD" data-title="USD">USD</option>
									<option value="EUR" data-title="EUR">EUR</option>
									<option value="GBR" data-title="GBR">GBR</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="header-mid">
			<div class="container">
				<div class="d-flex justify-content-between align-items-center">
					<a class="navbar-brand" href="">
						<img class="logo_dark" src="./img/logo_dark.png" alt="com-logo">
					</a>
					<div class="product_search_form d-none d-lg-block">
						<form>
							<div class="input-group">
								<div class="input-group-prepend">
									<div class="custom_select">
										<select class="first_null not_chosen">
											<option value="">All Category</option>
											<option value="Dresses">Dresses</option>
											<option value="Shirt-Tops">Shirt &amp; Tops</option>
											<option value="T-Shirt">T-Shirt</option>
											<option value="Pents">Pents</option>
											<option value="Jeans">Jeans</option>
										</select>
									</div>
								</div>
								<input class="form-control" placeholder="Search Product..." required="" type="text">
								<button type="submit" class="search_btn"><span class="lnr lnr-magnifier"></span></button>
							</div>
						</form>
					</div>
					<div class="contact_phone">
						<span class="lnr lnr-phone mr-2"></span>
						<span>123-456-7689</span>
					</div>
				</div>
			</div>
		</div>

		<div class="header-bottom mb-4">
			<div class="container">
				<div class="row flex-nowrap">
					<div class="col-3 col-sm-6 col-lg-3 col-md-4 ">
						<div class="categories_wrap">
							<button aria-expanded="false" class="collapsed">
								<span class="lnr lnr-menu"></span>
								<span class="d-sm-hide">All Categories</span>
							</button>
							<div id="navCatContent" class="nav-sm-lf " aria-expanded="false" nav-toggle="true">
								<ul class="bx-shadow">
									<li class="dropdown dropdown-mega-menu">
										<a class="dropdown-item nav-link dropdown-toggler" href="#" data-toggle="dropdown"><i class="flaticon-woman"></i> <span>Woman's</span></a>
										<div class="dropdown-menu ">
											<ul class="mega-menu d-lg-flex">
												<li class="mega-menu-col col-lg-7 pl-0 pl-lg-0">
													<ul class="d-lg-flex">
														<li class="mega-menu-col col-lg-6 pl-0 pl-lg-0">
															<ul>
																<li class="dropdown-header">Featured Item</li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Vestibulum sed</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec porttitor</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec vitae facilisis</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Curabitur tempus</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Vivamus in tortor</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec vitae ante ante</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Etiam ac rutrum</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Quisque condimentum</a></li>
															</ul>
														</li>
														<li class="mega-menu-col pl-0 col-lg-6">
															<ul>
																<li class="dropdown-header">Popular Item</li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Curabitur laoreet</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Vivamus in tortor</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec vitae facilisis</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Quisque condimentum</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Etiam ac rutrum</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec vitae ante ante</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec porttitor</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Curabitur tempus</a></li>
															</ul>
														</li>
													</ul>
												</li>
												<li class="mega-menu-col col-lg-5 pl-0 pl-lg-0">
													<div class="header-banner2">
														<img src="./img/menu_banner1.jpg" alt="menu_banner1">
														<div class="banne_info">
															<h6>10% Off</h6>
															<h4>New Arrival</h4>
															<a href="#">Shop now</a>
														</div>
													</div>
													<div class="header-banner2">
														<img src="./img/menu_banner2.jpg" alt="menu_banner2">
														<div class="banne_info">
															<h6>15% Off</h6>
															<h4>Men's Fashion</h4>
															<a href="#">Shop now</a>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li class="dropdown dropdown-mega-menu ">
										<a class="dropdown-item nav-link dropdown-toggler" href="#" data-toggle="dropdown"><i class="flaticon-boss"></i> <span>Men's</span></a>
										<div class="dropdown-menu">
											<ul class="mega-menu d-lg-flex">
												<li class="mega-menu-col col-lg-7 px-0">
													<ul class="d-lg-flex">
														<li class="mega-menu-col col-lg-6 px-0">
															<ul>
																<li class="dropdown-header">Featured Item</li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Vestibulum sed</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec porttitor</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec vitae facilisis</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Curabitur tempus</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Vivamus in tortor</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec vitae ante ante</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Etiam ac rutrum</a></li>
															</ul>
														</li>
														<li class="mega-menu-col col-lg-6 px-0">
															<ul>
																<li class="dropdown-header">Popular Item</li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Curabitur laoreet</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Vivamus in tortor</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec vitae facilisis</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Quisque condimentum</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Etiam ac rutrum</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec vitae ante ante</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec porttitor</a></li>
															</ul>
														</li>
													</ul>
												</li>
												<li class="mega-menu-col col-lg-5">
													<div class="header-banner2">
														<a href="#"><img src="./img/menu_banner4.jpg" alt="menu_banner4"></a>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li class="dropdown dropdown-mega-menu">
										<a class="dropdown-item nav-link dropdown-toggler" href="#" data-toggle="dropdown"><i class="flaticon-friendship"></i> <span>Kid's</span></a>
										<div class="dropdown-menu">
											<ul class="mega-menu d-lg-flex">
												<li class="mega-menu-col col-lg-7 px-0">
													<ul class="d-lg-flex">
														<li class="mega-menu-col col-lg-6 px-0">
															<ul>
																<li class="dropdown-header">Featured Item</li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Vestibulum sed</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec porttitor</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec vitae facilisis</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Curabitur tempus</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Vivamus in tortor</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec vitae ante ante</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Etiam ac rutrum</a></li>
															</ul>
														</li>
														<li class="mega-menu-col col-lg-6 px-0">
															<ul>
																<li class="dropdown-header">Popular Item</li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Curabitur laoreet</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Vivamus in tortor</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec vitae facilisis</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Quisque condimentum</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Etiam ac rutrum</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec vitae ante ante</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="#">Donec porttitor</a></li>
															</ul>
														</li>
													</ul>
												</li>
												<li class="mega-menu-col col-lg-5">
													<div class="header-banner2">
														<a href="#"><img src="./img/menu_banner5.jpg" alt="menu_banner5"></a>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li class="dropdown dropdown-mega-menu">
										<a class="dropdown-item nav-link dropdown-toggler" href="#" data-toggle="dropdown"><i class="flaticon-sunglasses"></i> <span>Accessories</span></a>
										<div class="dropdown-menu">
											<ul class="mega-menu d-lg-flex">
												<li class="mega-menu-col col-lg-4 px-0">
													<ul>
														<li class="dropdown-header">Woman's</li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-three-columns.html">Vestibulum sed</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-four-columns.html">Donec porttitor</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-grid-view.html">Donec vitae facilisis</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-list-view.html">Curabitur tempus</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-left-sidebar.html">Vivamus in tortor</a></li>
													</ul>
												</li>
												<li class="mega-menu-col col-lg-4 px-0">
													<ul>
														<li class="dropdown-header">Men's</li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-cart.html">Donec vitae ante ante</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="checkout.html">Etiam ac rutrum</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="wishlist.html">Quisque condimentum</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="compare.html">Curabitur laoreet</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="cart-empty.html">Vivamus in tortor</a></li>
													</ul>
												</li>
												<li class="mega-menu-col col-lg-4 px-0">
													<ul>
														<li class="dropdown-header">Kid's</li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail.html">Donec vitae facilisis</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-left-sidebar.html">Quisque condimentum</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-right-sidebar.html">Etiam ac rutrum</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-thumbnails-left.html">Donec vitae ante ante</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-accordion-style.html">Donec porttitor</a></li>
													</ul>
												</li>
											</ul>
										</div>
									</li>
									<li><a class="dropdown-item nav-link nav_item" href=""><i class="flaticon-jacket"></i> <span>Clothing</span></a></li>
									<li><a class="dropdown-item nav-link nav_item" href=""><i class="flaticon-sneakers"></i> <span>Shoes</span></a></li>
									<li><a class="dropdown-item nav-link nav_item" href=""><i class="flaticon-watch"></i> <span>Watches</span></a></li>
									<li><a class="dropdown-item nav-link nav_item" href=""><i class="flaticon-necklace"></i> <span>Jewellery</span></a></li>
									<li><a class="dropdown-item nav-link nav_item" href=""><i class="flaticon-herbal"></i> <span>Health &amp; Beauty</span></a></li>
									<li><a class="dropdown-item nav-link nav_item" href=""><i class="flaticon-ball"></i> <span>Sports</span></a></li>
									<li>
										<ul class="more_slide_open" style="display: none">
											<li><a class=" dropdown-item nav-link nav_item" href="login.html"><i class="flaticon-pijamas"></i> <span>Sleepwear</span></a>
											<li><a class="dropdown-item nav-link nav_item" href=""><i class="flaticon-scarf"></i> <span>Seasonal Wear</span></a></li>
											<li><a class="dropdown-item nav-link nav_item" href=""><i class="flaticon-vintage"></i> <span>Ethinic Wear</span></a></li>
											<li><a class="dropdown-item nav-link nav_item" href=""><i class="flaticon-pregnant"></i> <span>Baby Clothing</span></a></li>
										</ul>
									</li>
								</ul>
								<div class="more_categories bx-shadow">More Categories</div>
							</div>
						</div>
					</div>
					<div class="col-9 col-sm-6 col-lg-9 col-md-8 ">
						<nav class="navbar navbar-expand-lg ">
							<div class="pr_search_icon">
								<a href="#" class="nav-link pr_search_trigger"><span class="lnr lnr-magnifier"></span></a>
							</div>
							<div class="nav-side" id="navbarSidetoggle">
								<ul class="navbar-nav-sm">
									<li class="dropdown a">
										<a data-toggle="dropdown" class="nav-link drop-toggle active" href="#">Home</a>
										<div class="dropdown-menu">
											<ul>
												<li><a class="dropdown-item nav-link nav_item" href="">Fashion 1</a></li>
												<li><a class="dropdown-item nav-link nav_item active" href="">Fashion 2</a></li>
												<li><a class="dropdown-item nav-link nav_item" href="">Furniture 1</a></li>
												<li><a class="dropdown-item nav-link Xnav_item" href="">Furniture 2</a></li>
												<li><a class="dropdown-item nav-link nav_item" href="">Electronics 1</a></li>
												<li><a class="dropdown-item nav-link nav_item" href="">Electronics 2</a></li>
											</ul>
										</div>
									</li>
									<li class="dropdown">
										<a class=" nav-link drop-toggle" href="#" data-toggle="dropdown">Pages</a>
										<div class="dropdown-menu">
											<ul>
												<li><a class="dropdown-item nav-link nav_item" href="about.html">About Us</a></li>
												<li><a class="dropdown-item nav-link nav_item" href="contact.html">Contact Us</a></li>
												<li><a class="dropdown-item nav-link nav_item" href="faq.html">Faq</a></li>
												<li><a class="dropdown-item nav-link nav_item" href="404.html">404 Error Page</a></li>
												<li><a class="dropdown-item nav-link nav_item" href="login.php">Login</a></li>
												<li><a class="dropdown-item nav-link nav_item" href="signup.php">Register</a></li>
												<li><a class="dropdown-item nav-link nav_item" href="term-condition.html">Terms and Conditions</a></li>
											</ul>
										</div>
									</li>
									<li class="dropdown dropdown-mega-menu">
										<a class="drop-toggle nav-link" href="#" data-toggle="dropdown">Products</a>
										<div class="dropdown-menu">
											<ul class="mega-menu d-lg-flex">
												<li class="mega-menu-col col-lg-3 px-lg-0">
													<ul>
														<li class="dropdown-header">Woman's</li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-three-columns.html">Vestibulum sed</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-four-columns.html">Donec porttitor</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-grid-view.html">Donec vitae facilisis</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-list-view.html">Curabitur tempus</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-left-sidebar.html">Vivamus in tortor</a></li>
													</ul>
												</li>
												<li class="mega-menu-col col-lg-3 px-lg-0">
													<ul>
														<li class="dropdown-header">Men's</li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-cart.html">Donec vitae ante ante</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="checkout.html">Etiam ac rutrum</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="wishlist.html">Quisque condimentum</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="compare.html">Curabitur laoreet</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="cart-empty.html">Vivamus in tortor</a></li>
													</ul>
												</li>
												<li class="mega-menu-col col-lg-3 px-lg-0">
													<ul>
														<li class="dropdown-header">Kid's</li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail.html">Donec vitae facilisis</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-left-sidebar.html">Quisque condimentum</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-right-sidebar.html">Etiam ac rutrum</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-thumbnails-left.html">Donec vitae ante ante</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-accordion-style.html">Donec porttitor</a></li>
													</ul>
												</li>
												<li class="mega-menu-col col-lg-3 px-lg-0">
													<ul>
														<li class="dropdown-header">Accessories</li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail.html">Curabitur tempus</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-left-sidebar.html">Quisque condimentum</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-right-sidebar.html">Vivamus in tortor</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-thumbnails-left.html">Donec vitae facilisis</a></li>
														<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-accordion-style.html">Donec porttitor</a></li>
													</ul>
												</li>
											</ul>
										</div>
									</li>
									<li class="dropdown">
										<a class="drop-toggle nav-link" href="#" data-toggle="dropdown">Blog</a>
										<div class="dropdown-menu dropdown-reverse">
											<ul>
												<li>
													<a class="dropdown-item menu-link dropdown-toggler" href="#">Grids</a>
													<div class="dropdown-menu">
														<ul>
															<li><a class="dropdown-item nav-link nav_item" href="blog-three-columns.html">3 columns</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-four-columns.html">4 columns</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-left-sidebar.html">Left Sidebar</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-right-sidebar.html">right Sidebar</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-standard-left-sidebar.html">Standard Left Sidebar</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-standard-right-sidebar.html">Standard right Sidebar</a></li>
														</ul>
													</div>
												</li>
												<li>
													<a class="dropdown-item menu-link dropdown-toggler" href="#">Masonry</a>
													<div class="dropdown-menu">
														<ul>
															<li><a class="dropdown-item nav-link nav_item" href="blog-masonry-three-columns.html">3 columns</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-masonry-four-columns.html">4 columns</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-masonry-left-sidebar.html">Left Sidebar</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-masonry-right-sidebar.html">right Sidebar</a></li>
														</ul>
													</div>
												</li>
												<li>
													<a class="dropdown-item menu-link dropdown-toggler" href="#">Single Post</a>
													<div class="dropdown-menu">
														<ul>
															<li><a class="dropdown-item nav-link nav_item" href="blog-single.html">Default</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-single-left-sidebar.html">left sidebar</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-single-slider.html">slider post</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-single-video.html">video post</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-single-audio.html">audio post</a></li>
														</ul>
													</div>
												</li>
												<li>
													<a class="dropdown-item menu-link dropdown-toggler" href="#">List</a>
													<div class="dropdown-menu">
														<ul>
															<li><a class="dropdown-item nav-link nav_item" href="blog-list-left-sidebar.html">left sidebar</a></li>
															<li><a class="dropdown-item nav-link nav_item" href="blog-list-right-sidebar.html">right sidebar</a></li>
														</ul>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li class="dropdown dropdown-mega-menu">
										<a class="nav-link drop-toggle" href="#" data-toggle="dropdown">Shop</a>
										<div class="dropdown-menu a">
											<ul class="mega-menu d-lg-flex">
												<li class="mega-menu-col col-lg-12">
													<ul class="d-lg-flex">
														<li class="mega-menu-col col-lg-4 px-lg-0">
															<ul>
																<li class="dropdown-header">Shop Page Layout</li>
																<li><a class="dropdown-item nav-link nav_item" href="shop-list.html">shop List view</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="shop-list-left-sidebar.html">shop List Left Sidebar</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="shop-list-right-sidebar.html">shop List Right Sidebar</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="leftSideBar.php">Left Sidebar</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="shop-right-sidebar.html">Right Sidebar</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="shop-load-more.html">Shop Load More</a></li>
															</ul>
														</li>
														<li class="mega-menu-col col-lg-4 pr-lg-0">
															<ul>
																<li class="dropdown-header">Other Pages</li>
																<li><a class="dropdown-item nav-link nav_item" href="cart.php">Cart</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="checkout.html">Checkout</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="wishlist.html">Wishlist</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="compare.html">compare</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="order-completed.html">Order Completed</a></li>
															</ul>
														</li>
														<li class="mega-menu-col col-lg-4 px-lg-0">
															<ul>
																<li class="dropdown-header">Product Pages</li>
																<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail.html">Default</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-left-sidebar.html">Left Sidebar</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-right-sidebar.html">Right Sidebar</a></li>
																<li><a class="dropdown-item nav-link nav_item" href="shop-product-detail-thumbnails-left.html">Thumbnails Left</a></li>
															</ul>
														</li>
													</ul>
												</li>
											</ul>
										</div>
									</li>
									<li><a class="nav-link nav_item" href="">Contact Us</a></li>
								</ul>
							</div>
							<ul class="navbar-nav attr-nav align-items-center">
								<li><a href="#"><span class="lnr lnr-user"></span></a></li>
								<li><a href="#"><span class="lnr lnr-heart"></span><span class="wishlist_count">0</span></a></li>
								<li class="dropdown cart_dropdown"><a class="cart_trigger" href="#" data-toggle="dropdown"><span class="lnr lnr-cart"></span><span class="cart_count">2</span></a>
									<div class="cart_box">
										<ul class="cart_list">
											<li>
												<a href="#"><img src="./img/cart_thamb1.jpg" alt="cart_thumb1"><span>Variable product 001</span></a>
												<span class="spa-price"><span class="cart_quantity"> 1 x <span class="cart_amount"> <span class="price_symbole">$</span></span>78.00</span></span>
												<a href="#" class="item_remove"><i class="fas fa-times"></i></a>
											</li>
											<li>
												<a href="#"><img src="./img//cart_thamb2.jpg" alt="cart_thumb2"><span>Ornare sed consequat</span></a>
												<span class="spa-price"><span class="cart_quantity"> 1 x <span class="cart_amount"> <span class="price_symbole">$</span></span>81.00</span></span>
												<a href="#" class="item_remove"><i class="fas fa-times"></i></a>
											</li>
										</ul>
										<div class="cart_footer">
											<p class="cart_total"><strong>Subtotal:</strong> <span class="cart_price"> <span class="price_symbole">$</span>159.00</span></p>
											<p class="cart_buttons"><a href="#" class="btn btn-fill-line view-cart"><span>View Cart</span></a><a href="#" class="btn btn-fill-out checkout"><span>Checkout</span></a></p>
										</div>
									</div>
									<!-- dropdown-menu dropdown-menu-right -->
								</li>
							</ul>
							<button class="navbar-toggler side_navbar_toggler">
								<span class="lnr lnr-menu"></span>
							</button>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- HERO  SECTION -->
	<div class="banner_section slide_medium shop_banner_slider ">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 offset-lg-3">
					<div id="carouselExampleControls" class="carousel slide light_arrow" data-ride="carousel">
						<div class="carousel-inner">
							<div class="carousel-item background_bg active" data-img-src="img/banner4.jpg" style="background-image: url(&quot;img/banner4.jpg&quot;);">
								<div class="banner_slide_content banner_content_inner">
									<div class="col-lg-8 col-10">
										<div class="banner_content overflow-hidden">
											<h5 class="mb-3 staggered-animation font-weight-light animated slideInLeft" data-animation="slideInLeft" data-animation-delay="0.5s" style="animation-delay: 0.5s; opacity: 1;">Get up to 50% off Today Only!</h5>
											<h2 class="staggered-animation animated slideInLeft" data-animation="slideInLeft" data-animation-delay="1s" style="animation-delay: 1s; opacity: 1;">Woman Fashion</h2>
											<a class="btn btn-fill-out rounded-0 staggered-animation text-uppercase animated slideInLeft" href="shop-left-sidebar.html" data-animation="slideInLeft" data-animation-delay="1.5s" style="animation-delay: 1.5s; opacity: 1;"><span>Shop Now</span></a>
										</div>
									</div>
								</div>
							</div>
							<div class="carousel-item background_bg" data-img-src="img/banner5.jpg" style="background-image: url(&quot;img/banner5.jpg&quot;);">
								<div class="banner_slide_content banner_content_inner">
									<div class="col-lg-8 col-10">
										<div class="banner_content overflow-hidden">
											<h5 class="mb-3 staggered-animation font-weight-light animated slideInLeft" data-animation="slideInLeft" data-animation-delay="0.5s" style="animation-delay: 0.5s; opacity: 1;">50% off in all products</h5>
											<h2 class="staggered-animation animated slideInLeft" data-animation="slideInLeft" data-animation-delay="1s" style="animation-delay: 1s; opacity: 1;">Man Fashion</h2>
											<a class="btn btn-fill-out rounded-0 staggered-animation text-uppercase animated slideInLeft" href="shop-left-sidebar.html" data-animation="slideInLeft" data-animation-delay="1.5s" style="animation-delay: 1.5s; opacity: 1;"><span>Shop Now</span></a>
										</div>
									</div>
								</div>
							</div>
							<div class="carousel-item background_bg" data-img-src="img/banner6.jpg" style="background-image: url(&quot;img/banner6.jpg&quot;);">
								<div class="banner_slide_content banner_content_inner">
									<div class="col-lg-8 col-10">
										<div class="banner_content overflow-hidden">
											<h5 class="mb-3 staggered-animation font-weight-light animated slideInLeft" data-animation="slideInLeft" data-animation-delay="0.5s" style="animation-delay: 0.5s; opacity: 1;">Taking your Viewing Experience to Next Level</h5>
											<h2 class="staggered-animation animated slideInLeft" data-animation="slideInLeft" data-animation-delay="1s" style="animation-delay: 1s; opacity: 1;">Summer Sale</h2>
											<a class="btn btn-fill-out rounded-0 staggered-animation text-uppercase animated slideInLeft" href="shop-left-sidebar.html" data-animation="slideInLeft" data-animation-delay="1.5s" style="animation-delay: 1.5s; opacity: 1;"><span>Shop Now</span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<ol class="carousel-indicators indicators_style1">
							<li data-target="#carouselExampleControls" data-slide-to="0" class="active"></li>
							<li data-target="#carouselExampleControls" data-slide-to="1" class=""></li>
							<li data-target="#carouselExampleControls" data-slide-to="2" class=""></li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- MAIN CONTENT -->
	<div class="main_content">
		<div class="section small_pb">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="heading_tab_header">
							<div class="heading_s2">
								<h2>Exclusive Products</h2>
							</div>
							<div class="tab-style2">
								<button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" style="padding: 0 3px 10px 0">
									<i class="fas fa-bars"></i>
								</button>
								<ul class=" nav-tabs d-flex justify-content-start justify-content-lg-between " id="tabmenubar" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" id="arrival-tab" data-toggle="tab" href="#arrival" role="tab" aria-controls="arrival" aria-selected="true">New Arrival</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="sellers-tab" data-toggle="tab" href="#sellers" role="tab" aria-controls="sellers" aria-selected="false">Best Sellers</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="featured-tab" data-toggle="tab" href="#featured" role="tab" aria-controls="featured" aria-selected="false">Featured</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="special-tab" data-toggle="tab" href="#special" role="tab" aria-controls="special" aria-selected="false">Special Offer
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<!-- OWL CAROUSEL  ONE-->
				<div class="row">
					<div class="col-12 px-0">
						<div class="owl-one owl-carousel owl-theme">
							<?php
								$exclusive = 1;
								$stm = $pdo->prepare('SELECT * FROM prodcts WHERE exclusive = :exclusive');
								$stm->execute(['exclusive'=>$exclusive]);
								
                                while($exclusive_row =  $stm->fetch()){ ?>
                                    <div class="card hover-dark mx-3">
                                        <div class="card-img-wrap">
                                            <img src="<?php echo $exclusive_row->image ?>" alt="product_img5 " class="card-img-top">
                                            <div>
                                                <ul class="list_none pr_action_btn">
                                                    <li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
                                                    <li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
                                                    <li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
                                                    <li><a href="#"><i class="far fa-heart"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body product_info">
                                            <h6 class="product_title"><a href="shop-product-detail.html"><?php echo $exclusive_row->title ?></a></h6>
                                            <div class="product_price">
                                                <span class="price">$<?php echo $exclusive_row->price ?></span>
                                                <del>$<?php echo $exclusive_row->cancelled_price ?></del>
                                                <div class="on_sale">
                                                    <span><?php echo $exclusive_row->discount ?>% Off</span>
                                                </div>
                                            </div>
                                            <div class="rating_wrap">
                                                <div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                    <!-- <i class="fas fa-star-half-alt"></i> -->
                                                </div>
                                                <span class="rating_num">(21)</span>
                                            </div>
                                            <div class="pr_switch_wrap">
                                                <div class="product_color_switch">
                                                    <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                                                    <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                                                    <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                             <?php
                                }
                            ?>

							<div class="card hover-dark mx-3">
								<div class="card-img-wrap">
									<img src="./img/product_img2.jpg" alt="product_img5 " class="card-img-top">
									<div>
										<ul class="list_none pr_action_btn">
											<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
											<li><a href="#"><i class="far fa-heart"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-body product_info">
									<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
									<div class="product_price">
										<span class="price">$45.00</span>
										<del>$55.25</del>
										<div class="on_sale">
											<span>35% Off</span>
										</div>
									</div>
									<div class="rating_wrap">
										<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star-half-alt"></i>
											<i class="far fa-star"></i>
										</div>
										<span class="rating_num">(21)</span>
									</div>
									<div class="pr_switch_wrap">
										<div class="product_color_switch">
											<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
											<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
											<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="card hover-dark mx-3">
								<div class="card-img-wrap">
									<img src="./img/product_img3.jpg" alt="product_img5 " class="card-img-top">
									<div>
										<ul class="list_none pr_action_btn">
											<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
											<li><a href="#"><i class="far fa-heart"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-body product_info">
									<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
									<div class="product_price">
										<span class="price">$45.00</span>
										<del>$55.25</del>
										<div class="on_sale">
											<span>35% Off</span>
										</div>
									</div>
									<div class="rating_wrap">
										<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star-half-alt"></i>
										</div>
										<span class="rating_num">(21)</span>
									</div>
									<div class="pr_switch_wrap">
										<div class="product_color_switch">
											<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
											<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
											<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="card hover-dark mx-3">
								<div class="card-img-wrap">
									<img src="./img/product_img4.jpg" alt="product_img5 " class="card-img-top">
									<div>
										<ul class="list_none pr_action_btn">
											<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
											<li><a href="#"><i class="far fa-heart"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-body product_info">
									<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
									<div class="product_price">
										<span class="price">$45.00</span>
										<del>$55.25</del>
										<div class="on_sale">
											<span>35% Off</span>
										</div>
									</div>
									<div class="rating_wrap">
										<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star-half-alt"></i>
											<i class="far fa-star"></i>
										</div>
										<span class="rating_num">(21)</span>
									</div>
									<div class="pr_switch_wrap">
										<div class="product_color_switch">
											<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
											<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
											<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="card hover-dark mx-3">
								<div class="card-img-wrap">
									<img src="./img/product_img5.jpg" alt="product_img5 " class="card-img-top">
									<div>
										<ul class="list_none pr_action_btn">
											<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
											<li><a href="#"><i class="far fa-heart"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-body product_info">
									<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
									<div class="product_price">
										<span class="price">$45.00</span>
										<del>$55.25</del>
										<div class="on_sale">
											<span>35% Off</span>
										</div>
									</div>
									<div class="rating_wrap">
										<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="far fa-star"></i>
										</div>
										<span class="rating_num">(21)</span>
									</div>
									<div class="pr_switch_wrap">
										<div class="product_color_switch">
											<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
											<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
											<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="card hover-dark mx-3">
								<div class="card-img-wrap">
									<img src="./img/product_img6.jpg" alt="product_img5 " class="card-img-top">
									<div>
										<ul class="list_none pr_action_btn">
											<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
											<li><a href="#"><i class="far fa-heart"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-body product_info">
									<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
									<div class="product_price">
										<span class="price">$45.00</span>
										<del>$55.25</del>
										<div class="on_sale">
											<span>35% Off</span>
										</div>
									</div>
									<div class="rating_wrap">
										<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star-half-alt"></i>
											<i class="far fa-star"></i>
										</div>
										<span class="rating_num">(21)</span>
									</div>
									<div class="pr_switch_wrap">
										<div class="product_color_switch">
											<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
											<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
											<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="card hover-dark mx-3">
								<div class="card-img-wrap">
									<img src="./img/product_img7.jpg" alt="product_img5 " class="card-img-top">
									<div>
										<ul class="list_none pr_action_btn">
											<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
											<li><a href="#"><i class="far fa-heart"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-body product_info">
									<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
									<div class="product_price">
										<span class="price">$45.00</span>
										<del>$55.25</del>
										<div class="on_sale">
											<span>35% Off</span>
										</div>
									</div>
									<div class="rating_wrap">
										<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="far fa-star"></i>
											<!-- <i class="fas fa-star-half-alt"></i> -->
										</div>
										<span class="rating_num">(21)</span>
									</div>
									<div class="pr_switch_wrap">
										<div class="product_color_switch">
											<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
											<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
											<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="card hover-dark mx-3">
								<div class="card-img-wrap">
									<img src="./img/product_img8.jpg" alt="product_img5 " class="card-img-top">
									<div>
										<ul class="list_none pr_action_btn">
											<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
											<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
											<li><a href="#"><i class="far fa-heart"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-body product_info">
									<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
									<div class="product_price">
										<span class="price">$45.00</span>
										<del>$55.25</del>
										<div class="on_sale">
											<span>35% Off</span>
										</div>
									</div>
									<div class="rating_wrap">
										<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star-half-alt"></i>
										</div>
										<span class="rating_num">(21)</span>
									</div>
									<div class="pr_switch_wrap">
										<div class="product_color_switch">
											<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
											<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
											<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="section py-4 small_pt">
			<div class="container">
				<div class="row">
					<div class="col-md-6 mb-4">
						<div class="single_banner">
							<img src="./img/shop_banner_img1.jpg" alt="shop_banner_img1">
							<div class="single_banner_info">
								<h5 class="single_bn_title1">Super Sale</h5>
								<h3 class="single_bn_title-x">New Collection</h3>
								<a href="#" class="single_bn_link"><span>Shop Now</span></a>
							</div>
						</div>
					</div>
					<div class="col-md-6 mb-4">
						<div class="single_banner">
							<img src="./img/shop_banner_img2.jpg" alt="shop_banner_img2">
							<div class="single_banner_info">
								<h3 class="single_bn_title">New Season</h3>
								<h5 class="single_bn_title1">Sale 40% Off</h5>
								<a href="#" class="single_bn_link"><span>Shop Now</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- OWL CAROUSEL 2 -->
		<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="heading_tab_header">
							<div class="heading_s2">
								<h2>Deal Of The Days</h2>
							</div>
							<div class="deal_timer">
								<div class="countdown_time countdown_style1">
									<div class="countdown_box">
										<i class="far fa-clock"></i>
										<div class="countdown-wrap"><span class="countdown"></span></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- START OF OWL CAROUSEL 2 -->
				<div class="owl-wrap ">
					<div class="row">
						<div class="col-12 px-0">
							<div class="owl-one owl-carousel owl-theme">
								<div class="card hover-dark mx-3">
									<div class="card-img-wrap">
										<img src="./img/product_img1.jpg" alt="product_img5 " class="card-img-top">
										<div>
											<ul class="list_none pr_action_btn">
												<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
												<li><a href="#"><i class="far fa-heart"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="card-body product_info">
										<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
										<div class="product_price">
											<span class="price">$45.00</span>
											<del>$55.25</del>
											<div class="on_sale">
												<span>35% Off</span>
											</div>
										</div>
										<div class="rating_wrap">
											<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="far fa-star"></i>
												<!-- <i class="fas fa-star-half-alt"></i> -->
											</div>
											<span class="rating_num">(21)</span>
										</div>
										<div class="pr_switch_wrap">
											<div class="product_color_switch">
												<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
												<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
												<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="card hover-dark mx-3">
									<div class="card-img-wrap">
										<img src="./img/product_img2.jpg" alt="product_img5 " class="card-img-top">
										<div>
											<ul class="list_none pr_action_btn">
												<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
												<li><a href="#"><i class="far fa-heart"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="card-body product_info">
										<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
										<div class="product_price">
											<span class="price">$45.00</span>
											<del>$55.25</del>
											<div class="on_sale">
												<span>35% Off</span>
											</div>
										</div>
										<div class="rating_wrap">
											<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star-half-alt"></i>
												<i class="far fa-star"></i>
											</div>
											<span class="rating_num">(21)</span>
										</div>
										<div class="pr_switch_wrap">
											<div class="product_color_switch">
												<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
												<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
												<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="card hover-dark mx-3">
									<div class="card-img-wrap">
										<img src="./img/product_img3.jpg" alt="product_img5 " class="card-img-top">
										<div>
											<ul class="list_none pr_action_btn">
												<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
												<li><a href="#"><i class="far fa-heart"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="card-body product_info">
										<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
										<div class="product_price">
											<span class="price">$45.00</span>
											<del>$55.25</del>
											<div class="on_sale">
												<span>35% Off</span>
											</div>
										</div>
										<div class="rating_wrap">
											<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star-half-alt"></i>
											</div>
											<span class="rating_num">(21)</span>
										</div>
										<div class="pr_switch_wrap">
											<div class="product_color_switch">
												<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
												<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
												<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="card hover-dark mx-3">
									<div class="card-img-wrap">
										<img src="./img/product_img4.jpg" alt="product_img5 " class="card-img-top">
										<div>
											<ul class="list_none pr_action_btn">
												<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
												<li><a href="#"><i class="far fa-heart"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="card-body product_info">
										<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
										<div class="product_price">
											<span class="price">$45.00</span>
											<del>$55.25</del>
											<div class="on_sale">
												<span>35% Off</span>
											</div>
										</div>
										<div class="rating_wrap">
											<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star-half-alt"></i>
												<i class="far fa-star"></i>
											</div>
											<span class="rating_num">(21)</span>
										</div>
										<div class="pr_switch_wrap">
											<div class="product_color_switch">
												<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
												<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
												<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="card hover-dark mx-3">
									<div class="card-img-wrap">
										<img src="./img/product_img5.jpg" alt="product_img5 " class="card-img-top">
										<div>
											<ul class="list_none pr_action_btn">
												<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
												<li><a href="#"><i class="far fa-heart"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="card-body product_info">
										<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
										<div class="product_price">
											<span class="price">$45.00</span>
											<del>$55.25</del>
											<div class="on_sale">
												<span>35% Off</span>
											</div>
										</div>
										<div class="rating_wrap">
											<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="far fa-star"></i>
											</div>
											<span class="rating_num">(21)</span>
										</div>
										<div class="pr_switch_wrap">
											<div class="product_color_switch">
												<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
												<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
												<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="card hover-dark mx-3">
									<div class="card-img-wrap">
										<img src="./img/product_img6.jpg" alt="product_img5 " class="card-img-top">
										<div>
											<ul class="list_none pr_action_btn">
												<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
												<li><a href="#"><i class="far fa-heart"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="card-body product_info">
										<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
										<div class="product_price">
											<span class="price">$45.00</span>
											<del>$55.25</del>
											<div class="on_sale">
												<span>35% Off</span>
											</div>
										</div>
										<div class="rating_wrap">
											<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star-half-alt"></i>
												<i class="far fa-star"></i>
											</div>
											<span class="rating_num">(21)</span>
										</div>
										<div class="pr_switch_wrap">
											<div class="product_color_switch">
												<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
												<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
												<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="card hover-dark mx-3">
									<div class="card-img-wrap">
										<img src="./img/product_img7.jpg" alt="product_img5 " class="card-img-top">
										<div>
											<ul class="list_none pr_action_btn">
												<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
												<li><a href="#"><i class="far fa-heart"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="card-body product_info">
										<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
										<div class="product_price">
											<span class="price">$45.00</span>
											<del>$55.25</del>
											<div class="on_sale">
												<span>35% Off</span>
											</div>
										</div>
										<div class="rating_wrap">
											<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="far fa-star"></i>
												<!-- <i class="fas fa-star-half-alt"></i> -->
											</div>
											<span class="rating_num">(21)</span>
										</div>
										<div class="pr_switch_wrap">
											<div class="product_color_switch">
												<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
												<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
												<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="card hover-dark mx-3">
									<div class="card-img-wrap">
										<img src="./img/product_img8.jpg" alt="product_img5 " class="card-img-top">
										<div>
											<ul class="list_none pr_action_btn">
												<li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
												<li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
												<li><a href="#"><i class="far fa-heart"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="card-body product_info">
										<h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
										<div class="product_price">
											<span class="price">$45.00</span>
											<del>$55.25</del>
											<div class="on_sale">
												<span>35% Off</span>
											</div>
										</div>
										<div class="rating_wrap">
											<div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star-half-alt"></i>
											</div>
											<span class="rating_num">(21)</span>
										</div>
										<div class="pr_switch_wrap">
											<div class="product_color_switch">
												<span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
												<span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
												<span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="py-md-4 d-flex justify-content-between align-items-center flex-wrap">
			<div class="col-12 col-md-4">
				<div class="sale_banner">
					<a class="hover_effect1" href="#">
						<img src="./img/shop_banner_img3.jpg" alt="shop_banner_img3">
					</a>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="sale_banner">
					<a class="hover_effect1" href="#">
						<img src="./img/shop_banner_img4.jpg" alt="shop_banner_img4">
					</a>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="sale_banner">
					<a class="hover_effect1" href="#">
						<img src="./img/shop_banner_img5.jpg" alt="shop_banner_img5">
					</a>
				</div>
			</div>
		</div>

		<div class="section my-4">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="heading_tab_header">
							<div class="heading_s2">
								<h2>Featured Products</h2>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class=" owl-two owl-carousel owl-theme">
                            <?php
                                foreach ($data as $col){
                                ?>
                                    <div class="item">
                                        <?php
                                        foreach ($col as $row){
                                            ?>
                                            <div class="product">
                                                <div class="product_img">
                                                    <a href="shop-product-detail.html">
                                                        <img src="<?php echo $row->image; ?>" alt="product_img3">
                                                    </a>
                                                </div>
                                                <div class="product_info">
                                                    <h6 class="product_title"><a href="shop-product-detail.html"><?php echo $row->title; ?></a></h6>
                                                    <div class="product_price">
                                                        <span class="price">$<?php echo $row->price; ?></span>
                                                        <del>$99.00</del>
                                                        <div class="on_sale">
                                                            <span><?php echo $row->discount; ?>% Off</span>
                                                        </div>
                                                    </div>
                                                    <div class="rating_wrap">
                                                        <div class="rating">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star-half-alt"></i>
                                                        </div>
                                                        <span class="rating_num">(25)</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                <?php
                                }
                            ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="section my-4">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="heading_tab_header">
							<div class="heading_s2">
								<h2>Our Brands</h2>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="owl-three owl-carousel owl-theme">
                            <?php
                                $sql = $pdo->query('SELECT * FROM brands');
                                while ($brand = $sql->fetch()){
                            ?>
                                <div class="item">
                                    <div class="cl_logo">
                                        <img src="<?php echo $brand->brand_image; ?>" alt="cl_logo">
                                    </div>
                                </div>
                            <?php
                                }
                            ?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php require_once("./inc/footer.php") ?>

	<script src="jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="./owl.carousel.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ionicons/5.0.1/cjs/core-beb31360.min.js"></script>
	<script src="./callquery.js"></script>
	<script>
		var countDownDate = new Date("Jan 5, 2021 15:37:25").getTime();

		var x = setInterval(function() {
			var now = new Date().getTime();
			var distance = countDownDate - now;
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);

			if (seconds < 10) {
				seconds = "0" + seconds;
			}
			if (minutes < 10) {
				minutes = "0" + minutes;
			}
			if (hours < 10) {
				hours = "0" + hours;
			}

			document.querySelector(".countdown").innerHTML = hours + " : " +
				minutes + " : " + seconds + " ";
			if (distance < 0) {
				clearInterval(x);
				document.getElementById("countdown").innerHTML = "EXPIRED";
			}

		}, 1000);
	</script>
</body>

</html>