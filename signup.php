<?php
include_once 'navbar.php';
?>

<!--Product Detail Left Sidebar-->
<div class="header3 " style="background-color: #f7f8fb; height: 168px; padding: 50px 0px 50px 0px;">
    <div class="container">
        <div class="con " style="height: 168px; display: flex; justify-content: space-between">
            <div class="sidebartext">
                <h1>Login</h1>
            </div>
            <div class="">
                <div class="leftnav" style="display: flex">
                    <div class="leftnavlink">
                        <a href="">Home</a>
                        <span><i class="fa fa-angle-right "></i></span>
                    </div>
                    <div class="leftnavlink">
                        <a href="">Pages</a>
                        <span><i class="fa fa-angle-right "></i></span>
                    </div>
                    <div class="leftnavlink">
                        <a href="">Shop Left Sidebar</a>
                        <span><i class="fa fa-angle-right "></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Main content-->
<div class="loginSection text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="loginContainer ">
                    <div class="loginInner">
                        <div class="loginHeader">
                            <h3>Login</h3>
                        </div>
                        <form action="">
                            <input class="loginInp" type="text" placeholder="Enter Your Name"><br>
                            <input class="loginInp" type="email" placeholder="Enter Your Email"><br>
                            <input class="loginInp" type="email" placeholder="Password"><br>
                            <input class="loginInp" type="password" placeholder="Confirm Password"><br>
                            <div class="loginfooter">
                                <div>
                                    <input type="checkbox">
                                    <span>I agree to terms & Policy</span>
                                </div>

                            </div>
                            <button class="btn inpb">Register</button>
                        </form>
                        <div class="loginSignUp">
                            Already have an account? <a href="signup.php">Log in</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
include_once 'footer.php';
?>


