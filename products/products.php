<?php
session_start();
include_once '../config/db.php';
if (!(isset($_SESSION['username']) && isset($_SESSION['password']))) {
    $_SESSION['error'] = 'This session has expired';
    header("Location: ./login.php");
};


?>
<!DOCTYPE html>
<html lang="en">
    
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Products Page</title>
    <script src="https://kit.fontawesome.com/dba3c70e5f.js"></script>
    <link href="./dist/plugin/bootstrap.min.css" rel="stylesheet">
    <link href="../dist/css/style.css" rel="stylesheet">
    <style>
        #page-wrapper{
            margin-left: 0px !important;
        }
        table{
            width: 100% !important;
        }

        .btn{
            padding: 10px 20px !important;
        }
        .btn a{
            text-decoration: none;
        }
    </style>
</head>

<body class="fix-header">

    <div id="wrapper">
        

        <div id="page-wrapper">
                <div class="row">
                    <div class="col-12">
                        <div class="white-box">
                            <h3 class="box-title">Create Product</h3>
                            <button type="submit" name="create" class="btn btn-primary"><a href="create_products.php">Create Product</a></button>
                            
                        </div>
                        
                        <div class="white-box">
                            <h3 class="box-title">Recently Added</h3>
                            <div class="alert alert-success" id="success">
                                
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>IMAGE</th>
                                            <th>TITLE</th>
                                            <th>PRICE</th>
                                            <th>DISCOUNT</th>
                                            <th>C-PRICE</th>
                                            <th>Featured</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $sql = $pdo->query('SELECT * FROM product');
                                            $count = 1;
                                            while($product = $sql->fetch()){ ?>
                                                <tr id="prod<?php echo $product->id ?>">
                                                    <td><?php echo $count ?></td>
                                                    <td class="txt-oflo"><img src="<?php echo $product->image ?>" width="100px" alt=""></td>
                                                    <td class="txt-oflo"><?php echo $product->title ?></td>
                                                    <td><?php echo $product->price ?></td>
                                                    <td class="txt-oflo"><?php echo $product->discount ?></td>
                                                    <td><span class="text-success"><?php echo $product->cancelled_price ?></span></td>
                                    
                                                    <td><a href="edit_product.php?edit=<?php echo $product->id; ?>"><span><i class="fa fa-edit"></i></span></a></td>
                                                    <td><a data-id="<?php echo $product->id; ?>" class="delete-products"><span><i class="fa fa-trash"></i></span></a></td>
                                                </tr>
                                               <?php  $count +=1; ?>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="../js/product/create.js"></script>
</body>

</html>