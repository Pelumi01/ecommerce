<?php 
     include_once '../config/db.php';

    if(isset($_GET['edit'])){
        $edit_id = $_GET['edit'];
    
        $sql = "SELECT * FROM product WHERE id = ?";
        $stm = $pdo->prepare($sql);
        $stm->execute([$edit_id]);
        $editR = $stm->fetch();
        
    
    }
?>

<!DOCTYPE html>
<html lang="en">
    
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Dashboard</title>
    <script src="https://kit.fontawesome.com/dba3c70e5f.js"></script>
    <link href="./dist/plugin/bootstrap.min.css" rel="stylesheet">
    <link href="../dist/css/style.css" rel="stylesheet">
    <style>
        #page-wrapper{
            margin-left: 0px !important;
        }
        .form-group {
            margin-bottom: 25px;
            display: flex;
            flex-direction: column;
        }
        .btn{
            padding: 10px 20px !important;
        }
        .btn a{
            text-decoration: none;
        }
        a{
            text-decoration: none;
        }
    </style>
</head>

<body class="fix-header">
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="white-box">
                            <h3 class="box-title">Edit Item</h3>
                            <div class="alert alert-success" id="success">
                                
                            </div>
                            <div class="alert alert-danger" id="danger">
                                
                            </div>
                            <form action="" method="POST" enctype='multipart/form-data'>
                                <div class="form-group" >
                                    <label for="exampleInputEmail1">Item Name</label>
                                    <?php
                                        $product_name = '';
                                        $product_price ='';
                                        $product_discount = '';
                                        $product_Cprice = '';
                                        if(isset($_GET['edit'])){
                                            $product_name = $editR->title;
                                            $product_price = $editR->price;
                                            $product_discount = $editR->discount;
                                            $product_Cprice = $editR->cancelled_price;

                                        }elseif(isset($_POST['item'])){
                                            $product_name = $editR->title;
                                            $product_price = $editR->price;
                                            $product_discount = $editR->discount;
                                            $product_Cprice = $editR->cancelled_price;

                                        }
                                    ?>
                                    <input type="text" id="item" name="item" value="<?php echo $product_name; ?>" class="form-control" aria-describedby="Item descriptin" placeholder="Item Name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Item Price</label>
                                    <input  type="text" id="price" name="price" value="<?php echo $product_price; ?>" class="form-control" aria-describedby="Actual Price" placeholder="Actual Price">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Canceled Price</label>
                                    <input type="text" id="cancleP" name="cancleP" class="form-control" value="<?php echo $product_Cprice; ?>"  aria-describedby="Canceled Price" placeholder="Canceled Price">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Percentage Discount</label>
                                    <input type="text" id="discount" name="discount" value="<?php echo $product_discount; ?>" class="form-control" aria-describedby="Percentage Off" placeholder="Percentage Off">
                                    <input type="hidden" id="id" name="id" value="<?php echo $editR->id; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Item Image</label>
                                    <input type="file" id="file" name="file"  class="form-control" aria-describedby="Item_Image" placeholder="Percentage Off">
                                </div>
                                <?php
                                
                                if(isset($_GET['edit'])){ ?>
                                    <a href="products.php" class="btn btn-primary">Cancel</a>
                                <?php
                                }
                                ?>
                                <button type="submit" id="submit_edit" name="editP" class="btn btn-primary">Edit Product </button>
                              
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="../js/product/edit.js"></script>
</body>

</html>