<?php 
    session_start();
    include_once '../config/db.php';

?>

<!DOCTYPE html>
<html lang="en">
    
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Dashboard</title>
    <script src="https://kit.fontawesome.com/dba3c70e5f.js"></script>
    <link href="./dist/plugin/bootstrap.min.css" rel="stylesheet">
    <link href="../dist/css/style.css" rel="stylesheet">
    <style>
        #page-wrapper{
            margin-left: 0px !important;
        }
        .form-group {
            margin-bottom: 25px;
            display: flex;
            flex-direction: column;
        }
        .btn{
            padding: 10px 20px !important;
        }
        .btn a{
            text-decoration: none;
        }
    </style>
</head>

<body class="fix-header">

    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="white-box">
                            <h3 class="box-title">Creat New Item</h3>
                            
                            <div class="alert alert-success" id="success">
                                
                            </div>
                            <div class="alert alert-danger" id="danger">
                                
                            </div>
                            <form action="" method="POST" enctype='multipart/form-data'>
                                <div class="form-group" >
                                    <label for="exampleInputEmail1">Item Name</label>

                                    <input type="text" id="item" value="" class="form-control" aria-describedby="Item descriptin" placeholder="Item Name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Item Price</label>
                                    <input  type="text" name="price" id="price" value="" class="form-control" aria-describedby="Actual Price" placeholder="Actual Price">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Canceled Price</label>
                                    <input type="text" name="cancleP" id="cancleP" class="form-control" value=""  aria-describedby="Canceled Price" placeholder="Canceled Price">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Percentage Discount</label>
                                    <input type="text" name="discount" id="discount" value="    " class="form-control" aria-describedby="Percentage Off" placeholder="Percentage Off">
                                    <input type="hidden" name="id" id="id" value="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Item Image</label>
                                    <input type="file" name="file" id="file" class="form-control" aria-describedby="Item_Image" placeholder="Percentage Off">
                                </div> 
                                
                                <button type="submit" id="submit" name="create" class="btn btn-primary">Add Product </button>
                                <button type="submit" id="submit" name="create" class="btn btn-primary"> <a href="products.php">Product Page </a></button>
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="../js/product/create.js"></script>
</body>

</html>