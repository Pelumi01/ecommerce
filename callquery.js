$(window).on('load', function () {
    $('#preloader').delay(2000).fadeOut(800);
});

$(document).ready(function () {
    $('.dropdown-toggler').click(function () {
        $(this).siblings('.dropdown-menu').toggle()
    });

    $('.more_categories').click(function () {
        $(this).toggleClass('rmv');
        $('.more_slide_open').toggle(200)
    });

    $('.collapsed').click(function () {
        $('#navCatContent').toggleClass('show');
    });

    $('.side_navbar_toggler').click(function () {
        $('.nav-side').toggleClass('show');
    })

    $('.widget_instafeed a').hover(function () {
        $(this).children('.insta_icon').toggleClass('show');
    });

    $('.cart_trigger').click(function () {
        $('.cart_box').toggle(100);
    });

    $('.navbar-toggler').click(function () {
        $(this).siblings('.nav-tabs').toggleClass('show');
    });

    $('.owl-one').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            483: {
                items: 2,
            },
            600: {
                items: 3,
            },
            1300: {
                items: 4,
            }
        }
    });

    $('.owl-two').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            767: {
                items: 2,
            },
            992: {
                items: 3,
            }
        }
    });

    $('.owl-three').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
            },
            482: {
                items: 3,
            },
            992: {
                items: 3,
            }
        }
    });
});