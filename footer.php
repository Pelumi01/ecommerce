<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/themify-icons@1.0.0/themify-icons/_icons.scss">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ti-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/leftsidebarLg.css">
    <link rel="stylesheet" href="css/leftsidebarMd.css">
    <link rel="stylesheet" href="css/leftsidebarSm.css">
    <title>Hello, world!</title>
</head>
<body>

<!--Subscribe Newsletter-->
<div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="subscribeHeading">
                    <h3>Subscribe Our Newsletter</h3>
                </div>
            </div>
            <div class="col-md-6">
                <div class="subscribeform">
                    <form action="">
                        <input type="email" placeholder="Enter Email Address">
                        <button class="btn btn-dark">Subscribe</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Footer-->
<div id="footer" class="footer">
    <div class="footT">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-3">
                    <div class="foot">
                        <div class="footLogo">
                            <img src="img/footLogo.png" alt="">
                        </div>
                        <p style="color: #fff; ">If you are going to use of Lorem Ipsum need to be sure there isn't hidden of text</p>
                    </div>
                    <div class="footLogo">
                        <div class="footicons">
                            <a href="">
                                <i class="fa fa-facebook-f"></i>
                            </a>
                            <a href="">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="">
                                <i class="fa fa-facebook-f"></i>
                            </a>
                            <a href="">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="">
                                <i class="fa fa-facebook-f"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-2">
                    <div class="footLogo">
                        <h6>Useful Links</h6>
                    </div>
                    <div class="footul">
                        <ul>
                            <li>
                                <a href="">About Us</a>
                            </li>
                            <li>
                                <a href="">FAQ</a>
                            </li>
                            <li>
                                <a href="">Location</a>
                            </li>
                            <li>
                                <a href="">Affilates</a>
                            </li>
                            <li>
                                <a href="">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-2">
                    <div class="footLogo">
                        <h6>Category</h6>
                    </div>
                    <div class="footul">
                        <ul>
                            <li>
                                <a href="">Men</a>
                            </li>
                            <li>
                                <a href="">Women</a>
                            </li>
                            <li>
                                <a href="">Kids</a>
                            </li>
                            <li>
                                <a href="">Best Sellers</a>
                            </li>
                            <li>
                                <a href="">New Arrival</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-2">
                    <div class="footLogo">
                        <h6>My Account</h6>
                    </div>
                    <div class="footul">
                        <ul>
                            <li>
                                <a href="">My Account</a>
                            </li>
                            <li>
                                <a href="">Discount</a>
                            </li>
                            <li>
                                <a href="">Returns</a>
                            </li>
                            <li>
                                <a href="">Orders History</a>
                            </li>
                            <li>
                                <a href="">Order Tracking</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-3">
                    <div class="footLogo">
                        <h6>Contact Info</h6>
                    </div>
                    <div class="footul">
                        <ul class="footcontact">
                            <li class="contact1">
                                <i class="ti-location-pin"></i>
                                <p>123 Street, Old Trafford, New South London , UK</p>
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                <a href=""> info@sitename.com</a>
                            </li>
                            <li>
                                <i class="ti-mobile"></i>
                                <p> + 457 789 789 65</p>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footbottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p style="color: #fff;">© 2020 All Rights Reserved by Bestwebcreator</p>
                </div>
                <div class="col-md-6">
                    <div class="footpay text-right">
                        <ul class="footcards">
                            <li>
                                <a href="">
                                    <img src="img/visa.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <img src="img/discover.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <img src="img/master_card.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <img src="img/paypal.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <img src="img/amarican_express.png" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="scrolltop" >
                    <i class="fa fa-angle-up"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="js/index.js"></script>

</body>
</html>


