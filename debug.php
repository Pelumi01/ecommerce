<div class="owl-carousel owl-theme">
    <div class="owl-item">
        <div class="card hover-dark">
            <div class="card-img-wrap">
                <img src="./img/product_img1.jpg" alt="product_img5 " class="card-img-top">
                <div>
                    <ul class="list_none pr_action_btn">
                        <li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
                        <li><a href="#"><i class="far fa-heart"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body product_info">
                <h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
                <div class="product_price">
                    <span class="price">$45.00</span>
                    <del>$55.25</del>
                    <div class="on_sale">
                        <span>35% Off</span>
                    </div>
                </div>
                <div class="rating_wrap">
                    <div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                        <!-- <i class="fas fa-star-half-alt"></i> -->
                    </div>
                    <span class="rating_num">(21)</span>
                </div>
                <div class="pr_switch_wrap">
                    <div class="product_color_switch">
                        <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                        <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                        <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-item">
        <div class="card hover-dark">
            <div class="card-img-wrap">
                <img src="./img/product_img2.jpg" alt="product_img5 " class="card-img-top">
                <div>
                    <ul class="list_none pr_action_btn">
                        <li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
                        <li><a href="#"><i class="far fa-heart"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body product_info">
                <h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
                <div class="product_price">
                    <span class="price">$45.00</span>
                    <del>$55.25</del>
                    <div class="on_sale">
                        <span>35% Off</span>
                    </div>
                </div>
                <div class="rating_wrap">
                    <div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                        <i class="far fa-star"></i>
                    </div>
                    <span class="rating_num">(21)</span>
                </div>
                <div class="pr_switch_wrap">
                    <div class="product_color_switch">
                        <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                        <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                        <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-item">
        <div class="card hover-dark">
            <div class="card-img-wrap">
                <img src="./img/product_img3.jpg" alt="product_img5 " class="card-img-top">
                <div>
                    <ul class="list_none pr_action_btn">
                        <li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
                        <li><a href="#"><i class="far fa-heart"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body product_info">
                <h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
                <div class="product_price">
                    <span class="price">$45.00</span>
                    <del>$55.25</del>
                    <div class="on_sale">
                        <span>35% Off</span>
                    </div>
                </div>
                <div class="rating_wrap">
                    <div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                    </div>
                    <span class="rating_num">(21)</span>
                </div>
                <div class="pr_switch_wrap">
                    <div class="product_color_switch">
                        <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                        <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                        <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-item">
        <div class="card hover-dark">
            <div class="card-img-wrap">
                <img src="./img/product_img4.jpg" alt="product_img5 " class="card-img-top">
                <div>
                    <ul class="list_none pr_action_btn">
                        <li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
                        <li><a href="#"><i class="far fa-heart"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body product_info">
                <h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
                <div class="product_price">
                    <span class="price">$45.00</span>
                    <del>$55.25</del>
                    <div class="on_sale">
                        <span>35% Off</span>
                    </div>
                </div>
                <div class="rating_wrap">
                    <div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                        <i class="far fa-star"></i>
                    </div>
                    <span class="rating_num">(21)</span>
                </div>
                <div class="pr_switch_wrap">
                    <div class="product_color_switch">
                        <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                        <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                        <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-item">
        <div class="card hover-dark">
            <div class="card-img-wrap">
                <img src="./img/product_img5.jpg" alt="product_img5 " class="card-img-top">
                <div>
                    <ul class="list_none pr_action_btn">
                        <li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
                        <li><a href="#"><i class="far fa-heart"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body product_info">
                <h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
                <div class="product_price">
                    <span class="price">$45.00</span>
                    <del>$55.25</del>
                    <div class="on_sale">
                        <span>35% Off</span>
                    </div>
                </div>
                <div class="rating_wrap">
                    <div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                    </div>
                    <span class="rating_num">(21)</span>
                </div>
                <div class="pr_switch_wrap">
                    <div class="product_color_switch">
                        <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                        <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                        <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-item">
        <div class="card hover-dark">
            <div class="card-img-wrap">
                <img src="./img/product_img6.jpg" alt="product_img5 " class="card-img-top">
                <div>
                    <ul class="list_none pr_action_btn">
                        <li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
                        <li><a href="#"><i class="far fa-heart"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body product_info">
                <h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
                <div class="product_price">
                    <span class="price">$45.00</span>
                    <del>$55.25</del>
                    <div class="on_sale">
                        <span>35% Off</span>
                    </div>
                </div>
                <div class="rating_wrap">
                    <div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                        <i class="far fa-star"></i>
                    </div>
                    <span class="rating_num">(21)</span>
                </div>
                <div class="pr_switch_wrap">
                    <div class="product_color_switch">
                        <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                        <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                        <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-item">
        <div class="card hover-dark">
            <div class="card-img-wrap">
                <img src="./img/product_img7.jpg" alt="product_img5 " class="card-img-top">
                <div>
                    <ul class="list_none pr_action_btn">
                        <li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
                        <li><a href="#"><i class="far fa-heart"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body product_info">
                <h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
                <div class="product_price">
                    <span class="price">$45.00</span>
                    <del>$55.25</del>
                    <div class="on_sale">
                        <span>35% Off</span>
                    </div>
                </div>
                <div class="rating_wrap">
                    <div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                        <!-- <i class="fas fa-star-half-alt"></i> -->
                    </div>
                    <span class="rating_num">(21)</span>
                </div>
                <div class="pr_switch_wrap">
                    <div class="product_color_switch">
                        <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                        <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                        <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-item">
        <div class="card hover-dark">
            <div class="card-img-wrap">
                <img src="./img/product_img8.jpg" alt="product_img5 " class="card-img-top">
                <div>
                    <ul class="list_none pr_action_btn">
                        <li class="add-to-cart"><a href="#"><i class="fas fa-cart-plus"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-random"></i></a></li>
                        <li><a href="" class="popup-ajax"><i class="fas fa-search-plus"></i></a></li>
                        <li><a href="#"><i class="far fa-heart"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body product_info">
                <h6 class="product_title"><a href="shop-product-detail.html">blue dress for woman</a></h6>
                <div class="product_price">
                    <span class="price">$45.00</span>
                    <del>$55.25</del>
                    <div class="on_sale">
                        <span>35% Off</span>
                    </div>
                </div>
                <div class="rating_wrap">
                    <div class="product_rate" style="color:#F6BC3E; margin-right: 1rem">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                    </div>
                    <span class="rating_num">(21)</span>
                </div>
                <div class="pr_switch_wrap">
                    <div class="product_color_switch">
                        <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                        <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                        <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>