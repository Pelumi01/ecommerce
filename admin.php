<?php
session_start();
if (!(isset($_SESSION['username']) && isset($_SESSION['password']))) {
    $_SESSION['error'] = 'This session has expired';
    header("Location: ./login.php");
};

// include_once './config/crud_handler.php';

?>
<!DOCTYPE html>
<html lang="en">
    
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Dashboard</title>
    <script src="https://kit.fontawesome.com/dba3c70e5f.js"></script>
    <link href="./dist/plugin/bootstrap.min.css" rel="stylesheet">
    <link href="./dist/css/style.css" rel="stylesheet">
    <style>
        .admin_log {
            position: relative;
        }

        .admin_log .drop-log {
            background: #bbb;
            position: absolute;
            top: 100%;
            right: 25%;
            height: 100px;
            width: 100px;
            padding: 10px;
            display: none;
        }
    </style>
</head>

<body class="fix-header">
    <div class="container">
        <div class="modal fade" id="logOut" tabindex="-1" role="dialog" aria-labelledby="logOutTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="logOutTitle">You are an admin</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        You are trying to log out of your current session
                    </div>
                    <div class="modal-footer" style="display: flex; align-items:flex-end; justify-content:flex-end">
                        <button type="button" class="btn btn-secondary">Close</button>
                        <form action="delete.php" method="POST" style="display: inline-block; width: 100%;">
                            <input type="hidden" name="user" value="<?php echo $_SESSION['username'] ?>">
                            <button type="submit" name="logout">Log Out</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <div class="top-left-part">
                    <a class="logo" href="#" style="color: #fff;">
                        <b>
                            <h3 style="color: #fff;">eCommerce</h3>
                        </b>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                        <a class="nav-toggler open-close waves-effect waves-light hidden-md hidden-lg" href="javascript:void(0)"><i class="fa fa-bars"></i></a>
                    </li>
                    <li>
                        <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                            <input type="text" placeholder="Search..." class="form-control">
                            <a href="">
                                <i class="fa fa-search"></i>
                            </a>
                        </form>
                    </li>
                    <li class="admin_log">
                        <a class="admin-ava" href=""> <i class="fas fa-user-tie img-circle"></i>
                            <b class="hidden-xs"><?php echo $_SESSION['username'] ?></b>
                        </a>
                        <div class="drop-log">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#logOut">
                                Log Out
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3>
                        <span class="fa-fw open-close">
                            <i class="ti-close ti-menu"></i>
                        </span>
                        <span class="hide-menu">Navigation</span>
                    </h3>
                </div>

                <a href="./delete.php?user=<?php echo $_SESSION['username'] ?>" class="btn btn-primary bg-secondary-ouline" data-dismiss="modal">Log Out </a>
                <ul class="nav" id="side-menu">
                    <li style="padding: 70px 0 0;">
                        <a href="" class="waves-effect">
                            <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Admin Dashboard
                        </a>
                    </li>
                    <li>
                        <a href="products/products.php" class="waves-effect">
                            <i class="fas fa-baby-carriage"></i>
                            Product Categories
                        </a>
                    </li>
                </ul>
            </div>

        </div>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Dashboard</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="#">Dashboard</a></li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        
                        <div class="white-box">
                            <h3 class="box-title">Recently Added</h3>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>CATEGORIES</th>
                                            <th>DATE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td class="txt-oflo">Products</td>
                                            <td class="txt-oflo">20-05,2020</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.admin_log').hover(function() {
                $('.drop-log').toggle(300);
            })
        });

        function consoleLog() {
            console.log("Clicked");
        }
    </script>
</body>

</html>