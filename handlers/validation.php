<?php
session_start();
class Validate{
    public static function required($value,$fieldname){
        
        if(empty($value)){
            $error = $fieldname." field is required";
            $response=[
                'status'=> 422,
                'message'=>$error,
            ];
            echo json_encode($response);
            exit;
        }

    }

    public static function letter($value,$fieldname){
        
        if(empty($value) || !preg_match("/^[a-zA-Z ]*$/",$value) ){
            $error = "Only letters and white space is required for the ".$fieldname;
            $response=[
                'status'=> 422,
                'message'=>$error,
            ];
            echo json_encode($response);
            exit;
        }

    }

    public static function number($value,$fieldname){
        
        if(empty($value) || !preg_match("/^[1-9]\d*(\.\d+)?$/",$value) ){
            $error = "Only number is required for the ".$fieldname;
            $response=[
                'status'=> 422,
                'message'=>$error,
            ];
            echo json_encode($response);
            exit;
        }

    }
}

?>