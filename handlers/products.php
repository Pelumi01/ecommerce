<?php 
 include_once '../config/db.php';
 include_once 'validation.php';
    
    
 if(isset($_GET['create-item'])){
     Validate::required($_POST['item'], 'title' );
     Validate::required($_POST['price'], 'Item Price');
     Validate::required($_POST['cancleP'], 'Cancelled Price');
     Validate::required($_POST['discount'], 'Percentage discount');
     Validate::letter($_POST['item'], 'title');
     Validate::number($_POST['price'], 'Item Price');
     Validate::number($_POST['cancleP'], 'Cancelled Price');
     Validate::number($_POST['discount'], 'Percentage discount');

    $item = $_POST['item'];
    $price = $_POST['price'];
    $cancleP = $_POST['cancleP'];
    $discount= $_POST['discount'];

    // Image Path
    if(count($_FILES) > 0 ){
        if($_FILES['file'] != ''){
            $img_storage = $_FILES['file']['tmp_name'];
            $random_name = mt_rand(1111,999999);
            $img_file = '/e-commerce/uploads/'.$random_name;
            $ext = explode('.',$_FILES['file']['name']);
            $img_extension=$ext[count($ext) - 1];
            $img_file = $img_file.'.'.$img_extension;
            move_uploaded_file($img_storage,'../uploads/'.$random_name.'.'.$img_extension);
        }
    }


    
    $sql = "SELECT * FROM product WHERE title = ?";
    $stm = $pdo->prepare($sql);
    $stm->execute([$item]);
    $count = $stm->rowCount();
    if($count >= 1){
        $error= "The Item Already Exit";
        $response=[
            'status'=> 422,
            'message'=>$error,
        ];
        echo json_encode($response);
    }else{
        $sql = "INSERT INTO product(title, price, discount, image, cancelled_price) VALUES(?, ?, ?, ?, ?)";
        $stm = $pdo->prepare($sql);
        $stm->execute([$item, $price, $discount, $img_file, $cancleP]);
        $success = "Item Added";
        $response=[
            'status'=> 200,
            'message'=>$success,
        ];
        echo json_encode($response);
    }

}

    // Edit
if(isset($_GET['edit'] )){
    Validate::required($_POST['item'], 'title');
    Validate::required($_POST['price'], 'Item Price');
    Validate::required($_POST['cancleP'], 'Cancelled Price');
    Validate::required($_POST['discount'], 'Percentage discount');
    Validate::letter($_POST['item'], 'title');
    Validate::number($_POST['price'], 'Item Price');
    Validate::number($_POST['cancleP'], 'Cancelled Price');
    Validate::number($_POST['discount'], 'Percentage discount');
   
    $item = $_POST['item'];
    $price = $_POST['price'];
    $cancleP = $_POST['cancleP'];
    $discount= $_POST['discount'];
    $e_id = $_GET['edit'];

    // var_dump($_GET);
    
    $img_file = '';
    // Image Path
    if(count($_FILES) > 0 ){
        if($_FILES['file'] != ''){
            // $item_img = $_FILES['item_img'];
            $img_storage = $_FILES['file']['tmp_name'];
            $random_name = mt_rand(1111,999999);
            $img_file = '/e-commerce/uploads/'.$random_name;
            $ext = explode('.',$_FILES['file']['name']);
            $img_extension=$ext[count($ext) - 1];
            $img_file = $img_file.'.'.$img_extension;
            move_uploaded_file($img_storage,'../uploads/'.$random_name.'.'.$img_extension);
        }
    }

    $sql = "SELECT * FROM product WHERE title = ? AND id != ?";
    $stm = $pdo->prepare($sql);
    $stm->execute([$item, $e_id]);
    $count = $stm->rowCount();
    if($count >= 1){
        $message = "The Item Already Exit";
        $response=[
            'status'=> 422,
            'message'=>$message,
        ];
        echo json_encode($response);
        exit;
    }else{
        if($img_file == ''){
            $sqle = "UPDATE product SET title = :title,  price = :price, discount = :discount, cancelled_price = :cancleP WHERE id = :id";
            $stme = $pdo->prepare($sqle);
            $stme->execute([
                'title'=>$item,
                'price'=>$price,
                'discount'=>$discount,
                'cancleP'=>$cancleP,
                'id'=>$e_id,
            ]);
        }else{
            $sqle = "UPDATE product SET title = :title,  price = :price, discount = :discount, image = :img_file, cancelled_price = :cancleP WHERE id = :id ";
            $stme = $pdo->prepare($sqle);
            $stme->execute([
                'title'=>$item,
                'price'=>$price,
                'discount'=>$discount,
                'img_file'=>$img_file,
                'cancleP'=>$cancleP,
                'id'=>$e_id,
            ]);
        }
        
        $stme = $pdo->prepare($sqle);
        $stme->execute([
            'title'=>$item,
            'price'=>$price,
            'discount'=>$discount,
            'img_file'=>$img_file,
            'cancleP'=>$cancleP,
            'id'=>$e_id,
        ]);
        $success = "Successfuly Edited";
        $response=[
            'status'=> 200,
            'message'=>$success,
        ];
        echo json_encode($response);
    }
    
}

// Delete Product

if(isset($_POST['delete'])){
    $delete_id = $_POST['delete'];
    
    $sql = "DELETE FROM product WHERE id = ?";
    $stm = $pdo->prepare($sql);
    $stm->execute([$delete_id]);
    $success = "Item Deleted Successfuly";
    $response=[
        'status'=> 200,
        'message'=>$success,
    ];
    echo json_encode($response);
}


?>