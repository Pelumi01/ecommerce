<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/themify-icons@1.0.0/themify-icons/_icons.scss">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ti-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="css/leftsidebarLg.css">
    <link rel="stylesheet" href="css/leftsidebarMd.css">
    <link rel="stylesheet" href="css/leftsidebarSm.css">
    <title>Hello, world!</title>
</head>
<body>
<div class="header1">
    <div class="container pl-0 pr-0">
        <div class="navbar1 text-center">
            <div class="nav1" >
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="dic" style="display: flex; ">
                            <div class="n1"  style="display: flex; align-items: center; font-size: 15px">
                                <img class="img1" src="img/eng.png" alt="">
                                <span class="nav-link" id="navbarDropdownMenuLink"  data-toggle="dropdown">English <i class="fa fa-angle-down ic"></i></span>
                                <div class="dropdown-menu dr shadow" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="#">
                                        <img class="img2" src="img/eng.png" alt="">
                                        <span>English</span>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <img class="img2" src="img/fn.png" alt="">
                                        <span>France</span>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <img class="img2" src="img/us.png" alt="">
                                        <span>United States</span>
                                    </a>
                                </div>
                            </div>
                            <div class="n2">
                                <span class="nav-link" id="navbarDropdownMenuLink " data-toggle="dropdown">USD<i class="fa fa-angle-down ic"></i></span>
                                <div class="dropdown-menu dr2 shadow" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="#">
                                        <span>USD</span></a>
                                    <a class="dropdown-item" href="#">
                                        <span>EUR</span>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <span>GBR</span>
                                    </a>
                                </div>

                            </div>
                            <div class="n3">
                                <span class="phone"><i class="ti-mobile"></i></span>
                                <span class="pnum" style="font-size: 15px;">123-456-7890</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="dic2" style="display: flex; color: #292b2c; ">
                            <div class="n4 d1" >
                                <a href="">
                                    <i class="ti-control-shuffle" style="color: #292b2c; "></i>
                                    <span style="color: #292b2c; ">Compare</span>
                                </a>
                            </div>
                            <div class="n5 d1">
                                <a href="">
                                    <i class="ti-heart" style="color: #292b2c; "></i>
                                    <span style="color: #292b2c; ">Wishlist</span>
                                </a>
                            </div>
                            <div class="n6 d1">
                                <a href="login.php">
                                    <i class="ti-user" style="color: #292b2c; "></i>
                                    <span style="color: #292b2c;">Login</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Nav Bar-->
<div class="header2">
    <div class="container pl-0 pr-0">
        <div class="n  ">
            <nav class="navbar navbar-expand-lg navbar-light" id="nav" style="padding: 0rem 1rem;">
                <a class="navbar-brand" href="#" style="padding: 10px 0px;"><img width="78%" src="img/logo_dark.png" alt=""></a>
                <button class="navbar-toggler navtog" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon tog" ></span>
                    <span class="close" style="font-size: 23px; display: none "><i class="fa fa-close" aria-hidden="true"></i></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <span>PAGES</span>
                                    <i class="fa fa-angle-down "></i>
                                </div>
                                <ul class="uldrop shadow u1" id="ul1">
                                    <li>
                                        <a href="">Fashion 1</a>
                                    </li>
                                    <li>
                                        <a href="">Fashion 1</a>
                                    </li>
                                    <li>
                                        <a href="">Furniture 1</a>
                                    </li>
                                    <li>
                                        <a href="">Furniture 1</a>
                                    </li>
                                    <li>
                                        <a href="">Electronics 1</a>
                                    </li>
                                    <li>
                                        <a href="">Electronics 1</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <span>PAGES</span>
                                    <i class="fa fa-angle-down "></i>
                                </div>
                                <ul class="uldrop shadow u1" id="ul2">
                                    <li>
                                        <a href="">Fashion 3</a>
                                    </li>
                                    <li>
                                        <a href="">Fashion 3    </a>
                                    </li>
                                    <li>
                                        <a href="">Furniture 1</a>
                                    </li>
                                    <li>
                                        <a href="">Furniture 1</a>
                                    </li>
                                    <li>
                                        <a href="">Electronics 1</a>
                                    </li>
                                    <li>
                                        <a href="">Electronics 1</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <span>Products</span>
                                    <i class="fa fa-angle-down "></i>
                                </div>
                                <ul class="ul3drop shadow u1" style="background-color: white; z-index: 999999" >
                                    <div class="">
                                        <div class="ulchild">
                                            <li class="col-3 ulchild2 ch2">
                                                <ul class="lichild">
                                                    <li>
                                                        <h6>WOMEN'S</h6>
                                                    </li>
                                                    <li><a href="">Vestibulum Sed</a></li>
                                                    <li><a href="">Donec Partito</a></li>
                                                    <li><a href="">Donec Vitae Facilisis</a></li>
                                                    <li><a href="">Curabitor Tempus</a></li>
                                                    <li> <a href="">Vivamus In Tor</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-3 ulchild2 ch2">
                                                <ul class="lichild">
                                                    <li>
                                                        <h6>WOMEN'S</h6>
                                                    </li>
                                                    <li><a href="">Vestibulum Sed</a></li>
                                                    <li><a href="">Donec Partito</a></li>
                                                    <li><a href="">Donec Vitae Facilisis</a></li>
                                                    <li><a href="">Curabitor Tempus</a></li>
                                                    <li> <a href="">Vivamus In Tor</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-3 ulchild2 ch2">
                                                <ul class="lichild">
                                                    <li>
                                                        <h6>WOMEN'S</h6>
                                                    </li>
                                                    <li><a href="">Vestibulum Sed</a></li>
                                                    <li><a href="">Donec Partito</a></li>
                                                    <li><a href="">Donec Vitae Facilisis</a></li>
                                                    <li><a href="">Curabitor Tempus</a></li>
                                                    <li> <a href="">Vivamus In Tor</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-3 ulchild2 ch2" id="lastul">
                                                <ul class="lichild">
                                                    <li>
                                                        <h6>WOMEN'S</h6>
                                                    </li>
                                                    <li><a href="">Vestibulum Sed</a></li>
                                                    <li><a href="">Donec Partito</a></li>
                                                    <li><a href="">Donec Vitae Facilisis</a></li>
                                                    <li><a href="">Curabitor Tempus</a></li>
                                                    <li> <a href="">Vivamus In Tor</a></li>
                                                </ul>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="ulchild menuimage">
                                            <div class="col-4 menuimg">
                                                <img src="img/menu_banner1.jpg" width="280px" alt="">
                                                <div class="bann-info">
                                                    <h6>10% Off</h6>
                                                    <h4>New Arrival</h4>
                                                    <a href="" class="bannanchor"><span>Shop now</span>
                                                        <div class="aline"></div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-4 menuimg">
                                                <img src="img/menu_banner2.jpg" width="280px" alt="">
                                                <div class="bann-info">
                                                    <h6>10% Off</h6>
                                                    <h4>Men's Fashion</h4>
                                                    <a href="" class="bannanchor"><span>Shop now</span>
                                                        <div class="aline"></div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-4 menuimg">
                                                <img src="img/menu_banner3.jpg" width="280px" alt="">
                                                <div class="bann-info">
                                                    <h6>10% Off</h6>
                                                    <h4>Kids Fashion</h4>
                                                    <a href="" class="bannanchor"><span>Shop now</span>
                                                        <div class="aline"></div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ul>
                            </div>
                        </li>
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <span>BLOGS</span>
                                    <i class="fa fa-angle-down "></i>
                                </div>
                                <ul class="ul4drop shadow u1" >
                                    <li>
                                        <a href="" ><span>Grid</span>
                                            <div style="bi">
                                                <i class="fa fa-angle-right rightarrow"></i>
                                                <i class="fa fa-angle-left leftarrow" ></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="blogLi">
                                        <a href="" ><span>Masonry</span>
                                            <div style="bi">
                                                <i class="fa fa-angle-right rightarrow"></i>
                                                <i class="fa fa-angle-left leftarrow" ></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="blogLi">
                                        <a href="" ><span>Simple Post</span>
                                            <div style="bi">
                                                <i class="fa fa-angle-right rightarrow"></i>
                                                <i class="fa fa-angle-left leftarrow" ></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="blogLi">
                                        <a href="" ><span>List</span>
                                            <div style="bi">
                                                <i class="fa fa-angle-right rightarrow"></i>
                                                <i class="fa fa-angle-left leftarrow" ></i>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <span>Products</span>
                                    <i class="fa fa-angle-down "></i>
                                </div>
                                <ul class="ul5drop shadow u1" style="background-color: white; z-index: 999999">
                                    <div class="">
                                        <div class="ulchild">
                                            <li class="col-3 ulchild2 ch2">
                                                <ul class="lichild">
                                                    <li>
                                                        <h6>SHOP PAGE LAYOUT</h6>
                                                    </li>
                                                    <li><a href="">Shop List View</a></li>
                                                    <li><a href="">Shop List Left Sidebar</a></li>
                                                    <li><a href="">Shop List Right Sidebar</a></li>
                                                    <li><a href="">Left Sidebar</a></li>
                                                    <li> <a href="">Right Sidebar</a></li>
                                                    <li><a href="">Shop Load More</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-3 ulchild2 ch2">
                                                <ul class="lichild li5">
                                                    <li>
                                                        <h6>OTHER PAGES</h6>
                                                    </li>
                                                    <li><a href="">Cart</a></li>
                                                    <li><a href="">Checkout</a></li>
                                                    <li><a href="">Wishlist</a></li>
                                                    <li><a href="">Compare</a></li>
                                                    <li> <a href="">Order Completed</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-3 ulchild2 ch2">
                                                <ul class="lichild li5">
                                                    <li>
                                                        <h6>PRODUCT PAGES</h6>
                                                    </li>
                                                    <li><a href=""> Default</a></li>
                                                    <li><a href="">Left Sidebar</a></li>
                                                    <li><a href="">Right Sidebar</a></li>
                                                    <li><a href="">Thumbnails Left</a></li>

                                                </ul>
                                            </li>
                                            <li class="col-3 ulchild5 ch2" id="lastul">
                                                <ul class="lichild5">
                                                    <div class=" m5" >
                                                        <div class="menu5">
                                                            <img src="img/shop_banner.jpg" width="202px" alt="">
                                                        </div>
                                                        <div class="menu5Info">
                                                            <h5>NEW COLLECTION</h5>
                                                            <h3>SALE 30% OFF</h3>
                                                            <div class= "but">

                                                                <a href="#" class="btn btn1">SHOP NOW</a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </ul>
                                            </li>
                                        </div>
                                    </div>
                                </ul>
                            </div>
                        </li>
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <span>CONTACT US</span>
                                    <i class="fa fa-angle-down "></i>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="navic" style="display: flex; ">
                    <div class="navsearch">
                        <a href="#"><i class="lnr lnr-magnifier"></i></a>
                    </div>
                    <div class="navcart" >
                        <a href="#">
                            <i class="lnr lnr-cart"></i>
                            <span class="cart_count">0</span>
                        </a>
                        <div class="cartdropdown">
                            <ul>
                                <li>
                                    <a href="#">
                                        <img src="img/cart_thamb1.jpg" alt="">
                                        Variable production 001
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-close"></i>
                                    </a>
                                    <span class="cartprice">
                                        1 x$78.00
                                    </span>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/cart_thamb2.jpg" alt="">
                                        Variable production 001
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-close"></i>
                                    </a>
                                    <span class="cartprice">
                                        1 x$78.00
                                    </span>
                                </li>
                            </ul>
                            <div class="cartfooter">
                                <div class="ctotal">
                                    <span class="subtotal">Subtoal:</span>
                                    <span>$159.00</span>
                                </div>
                                <div class="cbutton">
                                    <a href="#" class="btn btn-dark  vcart">View Cart</a>
                                    <a href="#" class="btn checkout">Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="searchlayer">
                    <div class="searchinner">
                        <div class="searchform">
                            <i class="fa fa-close clo"></i>
                            <form action="">
                                <input type="text" placeholder="Search">
                                <span><i class="fa fa-search inputsearch"></i></span>
                            </form>
                        </div>

                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>



<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="js/index.js">


</script>

</body>
</html>


