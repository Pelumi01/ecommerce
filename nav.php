<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/themify-icons@1.0.0/themify-icons/_icons.scss">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ti-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="leftSideBar.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Left Side Bar</title>
</head>
<body>

<div class="header2">
    <div class="container pl-0 pr-0">
        <div class="n  ">
            <nav class="navbar navbar-expand-lg navbar-light" id="nav" style="padding: 0rem 1rem;">
                <a class="navbar-brand" href="#" style="padding: 10px 20px;"><img width="78%" src="img/logo_dark.png" alt=""></a>
                <button class="navbar-toggler navtog" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <span>PAGES</span>
                                    <i class="fa fa-angle-down "></i>
                                </div>
                                <ul class="uldrop shadow u1" id="ul1">
                                    <li>
                                        <a href="">Fashion 1</a>
                                    </li>
                                    <li>
                                        <a href="">Fashion 1</a>
                                    </li>
                                    <li>
                                        <a href="">Furniture 1</a>
                                    </li>
                                    <li>
                                        <a href="">Furniture 1</a>
                                    </li>
                                    <li>
                                        <a href="">Electronics 1</a>
                                    </li>
                                    <li>
                                        <a href="">Electronics 1</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <span>PAGES</span>
                                    <i class="fa fa-angle-down "></i>
                                </div>
                                <ul class="uldrop shadow u1" id="ul2">
                                    <li>
                                        <a href="">Fashion 3</a>
                                    </li>
                                    <li>
                                        <a href="">Fashion 3    </a>
                                    </li>
                                    <li>
                                        <a href="">Furniture 1</a>
                                    </li>
                                    <li>
                                        <a href="">Furniture 1</a>
                                    </li>
                                    <li>
                                        <a href="">Electronics 1</a>
                                    </li>
                                    <li>
                                        <a href="">Electronics 1</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <span>Products</span>
                                    <i class="fa fa-angle-down "></i>
                                </div>
                                <ul class="ul3drop shadow u1" >
                                    <div class="">
                                        <div class="ulchild">
                                            <li class="col-3 ulchild2 ch2">
                                                <ul class="lichild">
                                                    <li>
                                                        <h6>WOMEN'S</h6>
                                                    </li>
                                                    <li><a href="">Vestibulum Sed</a></li>
                                                    <li><a href="">Donec Partito</a></li>
                                                    <li><a href="">Donec Vitae Facilisis</a></li>
                                                    <li><a href="">Curabitor Tempus</a></li>
                                                   <li> <a href="">Vivamus In Tor</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-3 ulchild2 ch2">
                                                <ul class="lichild">
                                                    <li>
                                                        <h6>WOMEN'S</h6>
                                                    </li>
                                                    <li><a href="">Vestibulum Sed</a></li>
                                                    <li><a href="">Donec Partito</a></li>
                                                    <li><a href="">Donec Vitae Facilisis</a></li>
                                                    <li><a href="">Curabitor Tempus</a></li>
                                                    <li> <a href="">Vivamus In Tor</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-3 ulchild2 ch2">
                                                <ul class="lichild">
                                                    <li>
                                                        <h6>WOMEN'S</h6>
                                                    </li>
                                                    <li><a href="">Vestibulum Sed</a></li>
                                                    <li><a href="">Donec Partito</a></li>
                                                    <li><a href="">Donec Vitae Facilisis</a></li>
                                                    <li><a href="">Curabitor Tempus</a></li>
                                                    <li> <a href="">Vivamus In Tor</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-3 ulchild2 ch2" id="lastul">
                                                <ul class="lichild">
                                                    <li>
                                                        <h6>WOMEN'S</h6>
                                                    </li>
                                                    <li><a href="">Vestibulum Sed</a></li>
                                                    <li><a href="">Donec Partito</a></li>
                                                    <li><a href="">Donec Vitae Facilisis</a></li>
                                                    <li><a href="">Curabitor Tempus</a></li>
                                                    <li> <a href="">Vivamus In Tor</a></li>
                                                </ul>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="ulchild menuimage">
                                            <div class="col-4 menuimg">
                                                <img src="img/menu_banner1.jpg" width="280px" alt="">
                                                <div class="bann-info">
                                                    <h6>10% Off</h6>
                                                    <h4>New Arrival</h4>
                                                    <a href="" class="bannanchor"><span>Shop now</span>
                                                        <div class="aline"></div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-4 menuimg">
                                                <img src="img/menu_banner2.jpg" width="280px" alt="">
                                                <div class="bann-info">
                                                    <h6>10% Off</h6>
                                                    <h4>Men's Fashion</h4>
                                                    <a href="" class="bannanchor"><span>Shop now</span>
                                                        <div class="aline"></div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-4 menuimg">
                                                <img src="img/menu_banner3.jpg" width="280px" alt="">
                                                <div class="bann-info">
                                                    <h6>10% Off</h6>
                                                    <h4>Kids Fashion</h4>
                                                    <a href="" class="bannanchor"><span>Shop now</span>
                                                        <div class="aline"></div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ul>
                            </div>
                        </li>
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <span>BLOGS</span>
                                    <i class="fa fa-angle-down "></i>
                                </div>
                                <ul class="ul4drop shadow u1" >
                                    <li>
                                        <a href="" ><span>Grid</span>
                                            <div style="bi">
                                                <i class="fa fa-angle-right rightarrow"></i>
                                                <i class="fa fa-angle-left leftarrow" ></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="blogLi">
                                        <a href="" ><span>Masonry</span>
                                            <div style="bi">
                                                <i class="fa fa-angle-right rightarrow"></i>
                                                <i class="fa fa-angle-left leftarrow" ></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="blogLi">
                                        <a href="" ><span>Simple Post</span>
                                            <div style="bi">
                                                <i class="fa fa-angle-right rightarrow"></i>
                                                <i class="fa fa-angle-left leftarrow" ></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="blogLi">
                                        <a href="" ><span>List</span>
                                            <div style="bi">
                                                <i class="fa fa-angle-right rightarrow"></i>
                                                <i class="fa fa-angle-left leftarrow" ></i>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <span>Products</span>
                                    <i class="fa fa-angle-down "></i>
                                </div>
                                <ul class="ul5drop shadow u1" >
                                    <div class="">
                                        <div class="ulchild">
                                            <li class="col-3 ulchild2 ch2">
                                                <ul class="lichild">
                                                    <li>
                                                        <h6>SHOP PAGE LAYOUT</h6>
                                                    </li>
                                                    <li><a href="">Shop List View</a></li>
                                                    <li><a href="">Shop List Left Sidebar</a></li>
                                                    <li><a href="">Shop List Right Sidebar</a></li>
                                                    <li><a href="">Left Sidebar</a></li>
                                                    <li> <a href="">Right Sidebar</a></li>
                                                    <li><a href="">Shop Load More</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-3 ulchild2 ch2">
                                                <ul class="lichild li5">
                                                    <li>
                                                        <h6>OTHER PAGES</h6>
                                                    </li>
                                                    <li><a href="">Cart</a></li>
                                                    <li><a href="">Checkout</a></li>
                                                    <li><a href="">Wishlist</a></li>
                                                    <li><a href="">Compare</a></li>
                                                    <li> <a href="">Order Completed</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-3 ulchild2 ch2">
                                                <ul class="lichild li5">
                                                    <li>
                                                        <h6>PRODUCT PAGES</h6>
                                                    </li>
                                                    <li><a href=""> Default</a></li>
                                                    <li><a href="">Left Sidebar</a></li>
                                                    <li><a href="">Right Sidebar</a></li>
                                                    <li><a href="">Thumbnails Left</a></li>

                                                </ul>
                                            </li>
                                            <li class="col-3 ulchild5 ch2" id="lastul">
                                                <ul class="lichild5">
                                                    <div class=" m5" >
                                                        <div class="menu5">
                                                            <img src="img/shop_banner.jpg" width="202px" alt="">
                                                        </div>
                                                        <div class="menu5Info">
                                                            <h5>NEW COLLECTION</h5>
                                                            <h3>SALE 30% OFF</h3>
                                                            <div class= "but">

                                                                   <a href="" class="btn btn1">SHOP NOW</a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </ul>
                                            </li>
                                        </div>
                                    </div>
                                </ul>
                            </div>
                        </li>
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <span>CONTACT US</span>
                                    <i class="fa fa-angle-down "></i>
                                </div>
                            </div>
                        </li>
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <i class="fa fa-search "></i>
                                </div>
                            </div>
                        </li>
                        <li class="li l1" id="li">
                            <div class="HOD">
                                <div class="pdiv">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
        $(document).ready(function () {
            $('.l1').hover(
                function(){ $(this).find('.u1').show() },
                function(){ $(this).find('.u1').hide() }
            );

            $('.blogLi').hover(
                function () { $(this).find('.leftarrow').show() },
                function () { $(this).find('.rightarrow').hide() }
            )

            var linewidth = $(".aline").width();
            $(".bannanchor").mouseenter(function(){

                $('.aline').animate({
                    width: "70"
                });
            }).mouseleave(function(){
                $('.aline').animate({
                    width: "29%"
                });
            });
        })
</script>
</body>
</html>