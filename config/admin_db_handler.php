<?php

class Logadmin
{
    protected static $dsn = 'mysql:host=localhost;dbname=ecommerce';

    public static function adminCheck($username, $password)
    {
        try {
            $dbh = new PDO(self::$dsn, 'root', '');
            $sth = $dbh->prepare('SELECT * FROM admin  WHERE username = ? AND password = ?');
            $sth->execute([$username, $password]);
            $row = $sth->rowCount();
            if ($row == 1) {
              header('location:admin.php');
            } else {
                $_SESSION['error'] = "Please check your details";
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
        }
    }
}