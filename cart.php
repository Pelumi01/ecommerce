<?php
include_once 'navbar.php';

?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/themify-icons@1.0.0/themify-icons/_icons.scss">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ti-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/leftsidebarLg.css">
    <link rel="stylesheet" href="css/leftsidebarMd.css">
    <link rel="stylesheet" href="css/leftsidebarSm.css">
    <title>Cart</title>
</head>
<body>

<!--Product Detail Left Sidebar-->
<div class="header3 " style="background-color: #f7f8fb; height: 168px; padding: 50px 0px 50px 0px;">
    <div class="container">
        <div class="con " style="height: 168px; display: flex; justify-content: space-between">
            <div class="sidebartext">
                <h1>Shopping Cart</h1>
            </div>
            <div class="">
                <div class="leftnav" style="display: flex">
                    <div class="leftnavlink">
                        <a href="">Home</a>
                        <span><i class="fa fa-angle-right "></i></span>
                    </div>
                    <div class="leftnavlink">
                        <a href="">Pages</a>
                        <span><i class="fa fa-angle-right "></i></span>
                    </div>
                    <div class="leftnavlink">
                        <a href="">Shop Left Sidebar</a>
                        <span><i class="fa fa-angle-right "></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Main Content-->
<div class="cartSection">
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="table">
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col"></th>
                        <th scope="col">Product</th>
                        <th scope="col">Price</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Total </th>
                        <th scope="col">Remove</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">
                                <a href="#">
                                    <img src="img/img1.jpg" width="100px" alt="">
                                </a>
                            </th>
                            <td>Blue Dress For Women</td>
                            <td>$45.00</td>
                            <td class="productQuantity">
                                <div class="pQuantity">
                                    <input type="button" value="-" class="minus">
                                    <input type="text" value="1" class="quan">
                                    <input type="button" value="+" class="plus">

                                </div>
                            </td>
                            <td>$90.00</td>
                            <td class="productRemove">
                                <a href="#">
                                    <i class="ti-close"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <a href="#">
                                    <img src="img/img3.jpg" width="100px" alt="">
                                </a>
                            </th>
                            <td>Blue Dress For Women</td>
                            <td>$45.00</td>
                            <td class="productQuantity">
                                <div class="pQuantity">
                                    <input type="button" value="-" class="minus">
                                    <input type="text" value="1" class="quan">
                                    <input type="button" value="+" class="plus">

                                </div>
                            </td>
                            <td>$90.00</td>
                            <td class="productRemove">
                                <a href="#">
                                    <i class="ti-close"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <a href="#">
                                    <img src="img/img2.jpg" width="100px" alt="">
                                </a>
                            </th>
                            <td>Blue Dress For Women</td>
                            <td>$45.00</td>
                            <td class="productQuantity">
                                <div class="pQuantity">
                                    <input type="button" value="-" class="minus">
                                    <input type="text" value="1" class="quan">
                                    <input type="button" value="+" class="plus">

                                </div>
                            </td>
                            <td>$90.00</td>
                            <td class="productRemove">
                                <a href="#">
                                    <i class="ti-close"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6" class="productFooter">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="product_coupon">
                                            <input type="text" placeholder="Enter Coupon Code">
                                            <div class="coupon_btn">
                                                <button class="btn inpb btn-sm" name="submit" type="submit"><span>Apply Coupon</span></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6  col-md-6" >
                                        <div class="cart_btn">
                                            <button class="btn btn-sm" type="submit">Update Cart</button>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="divider">
                <div class="divider_icon">
                    <i class="ti-shopping-cart-full"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
<!--        <div class="col-md-6">-->
<!--            <div class="shipping_container">-->
<!--                <h6>Calculate Shipping</h6>-->
<!--                <form action="">-->
<!--                    <div class="row">-->
<!--                        <div class="col-lg-12">-->
<!--                            <select name="options" id="">-->
<!--                                <option value="Choose an option">Choose an option</option>-->
<!--                                <option value="Aland Island">Aland Island</option>-->
<!--                                <option value="Afghanistan">Afghanistan</option>-->
<!--                                <option value="Albania">Albania</option>-->
<!--                                <option value="Aland Island">Aland Island</option>-->
<!--                                <option value="Afghanistan">Afghanistan</option>-->
<!--                            </select>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="row">-->
<!--                        <div class="col-lg-6"></div>-->
<!--                        <div class="col-lg-6"></div>-->
<!--                    </div>-->
<!--                </form>-->
<!--            </div>-->
<!--        </div>-->
        <div class="col-md-8">
            <div class="cartTotal_container">
                <div class="cartTotal_info">
                    <div class="cartHeader">
                        <h6>Cart Totals</h6>
                    </div>
                    <div class="cartTotal_table">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Cart Subtotal</td>
                                    <td class="ct">$349.00</td>
                                </tr>
                            <tr>
                                <td >Shipping</td>
                                <td class="ct">Free Shipping</td>

                            </tr>
                            <tr>
                                <td >Total</td>
                                <td class="ct">$349.00</td>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="cart_proceed">
                        <button class="btn inpb proceed_btn" name="submit" type="submit"><span>Proceed To Checkout</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<?php
include_once 'footer.php';
?>
</body>
</html>
