-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2020 at 11:36 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin_ecommerce', 123456);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `brand_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand_name`, `brand_image`) VALUES
(1, '', '/e-commerce/img/cl_logo1.png'),
(2, '', '/e-commerce/img/cl_logo4.png'),
(3, '', '/e-commerce/img/cl_logo5.png'),
(4, '', '/e-commerce/img/cl_logo6.png'),
(5, '', '/e-commerce/img/cl_logo1.png'),
(6, '', '/e-commerce/img/cl_logo2.png'),
(7, '', '/e-commerce/img/cl_logo4.png');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `parent`) VALUES
(1, 'Woman\'s', 0),
(2, 'Men\'s', 0),
(3, 'Kid\'s', 0),
(4, 'Accessories', 0),
(5, 'Clothing', 0),
(6, 'Shoes', 0),
(7, 'Watches', 0),
(8, 'Jewellery', 0),
(9, 'Health & Beauty', 0),
(10, 'Sports', 0),
(11, 'Sleep Wear', 0),
(12, 'Seasonal Wear', 0),
(13, 'Ethinic', 0),
(14, 'Baby Clothing', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `discount` int(10) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `categories` varchar(255) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `featured` int(11) DEFAULT NULL,
  `Exclusive` int(11) DEFAULT NULL,
  `Deals` int(11) DEFAULT NULL,
  `cancelled_price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `title`, `price`, `discount`, `image`, `categories`, `brand`, `featured`, `Exclusive`, `Deals`, `cancelled_price`) VALUES
(2, 'Men Blue Jeans', '69.00', 89, '/e-commerce/img/product_img8.jpg', '', 1, 1, 0, 1, '0.00'),
(3, 'Pink Dress For Women', '65.00', 80, '/e-commerce/img/product_img13.jpg', '', 1, 1, 1, 1, '0.00'),
(4, 'Red &Black Check Shirt', '55.00', 95, '/e-commerce/img/product_img10.jpg', '', 1, 1, 1, 1, '0.00'),
(5, 'White Black Line Dress', '68.00', 99, '/e-commerce/img/product_img7.jpg', '', 1, 1, 1, 1, '0.00'),
(6, 'Leather Grey Tuxedo', '55.00', 25, '/e-commerce/img/product_img2.jpg', '', 1, 1, 1, 1, '0.00'),
(7, 'Blue Casual Check Shirt', '55.00', 25, '/e-commerce/img/product_img6.jpg', '', 1, 1, 1, 0, '0.00'),
(8, 'Black T-shirt For Woman', '69.00', 20, '/e-commerce/img/product_img12.jpg', '', 1, 1, 0, 1, '0.00'),
(9, 'White Shirt For Man', '55.00', 30, '/e-commerce/img/product_img14.jpg', '', 1, 1, 1, 1, '0.00'),
(10, 'white Gown', '60.00', 60, '/e-commerce/uploads/197554.jpg', NULL, NULL, NULL, NULL, NULL, '30.00'),
(25, 'White Shirt ', '40.00', 40, '/e-commerce/uploads/611635.jpg', NULL, NULL, 1, NULL, NULL, '80.00'),
(26, 'White Shirt For Men', '40.00', 40, '/e-commerce/uploads/945022.jpg', NULL, NULL, 1, NULL, NULL, '80.00'),
(28, 'White Top', '40.00', 30, '/e-commerce/uploads/345940.jpg', NULL, NULL, 1, NULL, NULL, '65.00'),
(31, 'Red Check', '40.00', 50, '/e-commerce/uploads/655719.jpg', NULL, NULL, 1, NULL, NULL, '80.00'),
(41, 'Black Nice Gowncvf', '20.00', 39, '/e-commerce/uploads/383537.jpg', NULL, NULL, NULL, NULL, NULL, '80.00'),
(66, 'White Shirt dec', '60.00', 40, '/e-commerce/uploads/25455.jpg', NULL, NULL, NULL, NULL, NULL, '80.00'),
(70, 'White Shirt decde', '60.00', 40, '/e-commerce/uploads/342199.jpg', NULL, NULL, NULL, NULL, NULL, '80.00'),
(71, 'White Shirt decdez', '60.00', 40, '/e-commerce/uploads/555685.jpg', NULL, NULL, NULL, NULL, NULL, '80.00'),
(72, 'White Shirt decdezxs', '60.00', 40, '/e-commerce/uploads/751798.jpg', NULL, NULL, NULL, NULL, NULL, '80.00'),
(73, 'White Shirt decdezxsdef', '60.00', 40, '/e-commerce/uploads/285166.jpg', NULL, NULL, NULL, NULL, NULL, '80.00'),
(78, 'Blue Dress for Womanxxd', '60.00', 30, '/e-commerce/uploads/697271.jpg', NULL, NULL, NULL, NULL, NULL, '60.00'),
(79, 'Black Nice Gownccd', '40.00', 50, '/e-commerce/uploads/494418.jpg', NULL, NULL, NULL, NULL, NULL, '80.00'),
(80, 'Black Short Gownee', '60.00', 75, '/e-commerce/uploads/551223.jpg', NULL, NULL, NULL, NULL, NULL, '80.00'),
(84, 'Black Short Gownc', '69.00', 75, '/e-commerce/uploads/30274.jpg', NULL, NULL, NULL, NULL, NULL, '80.00'),
(87, 'Black Short dehh', '79.00', 100, '/e-commerce/uploads/781460.jpg', NULL, NULL, NULL, NULL, NULL, '80.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
