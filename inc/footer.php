<footer class="bg-gray">
    <div class="bg-d py-md-5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="heading_s1 mb-md-0">
                        <h3>Subscribe Our Newsletter</h3>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="newsletter_form">
                        <form>
                            <input type="text" required="" class="form-control rounded-0" placeholder="Enter Email Address">
                            <button type="submit" class="btn btn-fill-out rounded-0" name="submit" value="Submit"><span>Subscribe</span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_top pt-3 pt-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="widget">
                        <div class="footer_logo">
                            <a href="#"><img src="./img/logo_dark.png" alt="logo"></a>
                        </div>
                        <p class="mb-3">If you are going to use of Lorem Ipsum need to be sure there isn't anything hidden of text</p>
                        <ul class="contact_info">
                            <li>
                                <i class="ti-location-pin"></i>
                                <p>123 Street, Old Trafford, NewYork, USA</p>
                            </li>
                            <li>
                                <i class="ti-email"></i>
                                <a href="mailto:info@sitename.com">info@sitename.com</a>
                            </li>
                            <li>
                                <i class="ti-mobile"></i>
                                <p>+ 457 789 789 65</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="widget">
                        <h6 class="widget_title my-sm-3">Useful Links</h6>
                        <ul class="widget_links">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Location</a></li>
                            <li><a href="#">Affiliates</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="widget">
                        <h6 class="widget_title my-sm-3">My Account</h6>
                        <ul class="widget_links">
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Discount</a></li>
                            <li><a href="#">Returns</a></li>
                            <li><a href="#">Orders History</a></li>
                            <li><a href="#">Order Tracking</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="widget i">
                        <h6 class="widget_title my-sm-3">Instagram</h6>
                        <ul class="widget_instafeed">
                            <li><a href="#"><img src="./img/insta_img1.jpg" alt="insta_img"><span class="insta_icon"><i class="fab fa-instagram"></i></span></a></li>
                            <li><a href="#"><img src="./img/insta_img2.jpg" alt="insta_img"><span class="insta_icon"><i class="fab fa-instagram"></i></span></a></li>
                            <li><a href="#"><img src="./img/insta_img3.jpg" alt="insta_img"><span class="insta_icon"><i class="fab fa-instagram"></i></span></a></li>
                            <li><a href="#"><img src="./img/insta_img4.jpg" alt="insta_img"><span class="insta_icon"><i class="fab fa-instagram"></i></span></a></li>
                            <li><a href="#"><img src="./img/insta_img5.jpg" alt="insta_img"><span class="insta_icon"><i class="fab fa-instagram"></i></span></a></li>
                            <li><a href="#"><img src="./img/insta_img6.jpg" alt="insta_img"><span class="insta_icon"><i class="fab fa-instagram"></i></span></a></li>
                            <li><a href="#"><img src="./img/insta_img7.jpg" alt="insta_img"><span class="insta_icon"><i class="fab fa-instagram"></i></span></a></li>
                            <li><a href="#"><img src="./img/insta_img8.jpg" alt="insta_img"><span class="insta_icon"><i class="fab fa-instagram"></i></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="middle_footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="shopping_info">
                        <div class="row justify-content-center">
                            <div class="col-md-4 pl-0">
                                <div class="icon_box my-3">
                                    <div class="icon">
                                        <i class="flaticon-shipped"></i>
                                    </div>
                                    <div class="icon_box_content">
                                        <h5>Free Delivery</h5>
                                        <p>Phasellus blandit massa enim elit of passage varius nunc.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 pl-0">
                                <div class="icon_box my-3">
                                    <div class="icon">
                                        <i class="flaticon-money-back"></i>
                                    </div>
                                    <div class="icon_box_content">
                                        <h5>30 Day Returns Guarantee</h5>
                                        <p>Phasellus blandit massa enim elit of passage varius nunc.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 pl-0">
                                <div class="icon_box my-3">
                                    <div class="icon">
                                        <i class="flaticon-support"></i>
                                    </div>
                                    <div class="icon_box_content">
                                        <h5>27/4 Online Support</h5>
                                        <p>Phasellus blandit massa enim elit of passage varius nunc.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom_footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4">
                    <p class="mt-3 my-lg-0 text-center" style="color: #687188">© 2020 All Rights Reserved by Bestwebcreator</p>
                </div>
                <div class="col-lg-4 order-lg-first">
                    <div class="widget mb-lg-0">
                        <ul class="social_icons text-center text-lg-left">
                            <li><a href="#" class="sc_facebook"><i class="ion-social-facebook"></i></a></li>
                            <li><a href="#" class="sc_twitter"><i class="ion-social-twitter"></i></a></li>
                            <li><a href="#" class="sc_google"><i class="ion-social-googleplus"></i></a></li>
                            <li><a href="#" class="sc_youtube"><i class="ion-social-youtube-outline"></i></a></li>
                            <li><a href="#" class="sc_instagram"><i class="ion-social-instagram-outline"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <ul class="footer_payment text-center text-lg-right d-flex justify-content-center justify-content-lg-end align-items-center">
                        <li><a href="#"><img src="./img/visa.png" alt="visa"></a></li>
                        <li><a href="#"><img src="./img/discover.png" alt="discover"></a></li>
                        <li><a href="#"><img src="./img/master_card.png" alt="master_card"></a></li>
                        <li><a href="#"><img src="./img/paypal.png" alt="paypal"></a></li>
                        <li><a href="#"><img src="./img/amarican_express.png" alt="amarican_express"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>